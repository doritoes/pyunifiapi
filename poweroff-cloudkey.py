#!/usr/bin/env python3
""" power off Ubiquity Unifi CloudKey device
"""
import sys
import getpass
from pycloudkeyapi import pyCloudkeyApi
worklist = []
discovered = []
api = pyCloudkeyApi()
for a in sys.argv:
    if a == 'all':
        print('Discovering CloudKeys...')
        allkeys = api.discover()
        for k in allkeys:
            print('discovered: {}'.format(k['ip']))
            if k['ip'] not in discovered:
                discovered.append(k['ip'])
    else:
        if a not in worklist:
            worklist.append(a)
worklist.pop(0)
for d in discovered:
    if d not in worklist:
        worklist.append(d)
if not worklist:
    worklist.append(input('IP of CloudKey to reset to power off: '))
for w in worklist:
    print('Powering off CloudKey: ' + w)
    creds = api.get_creds(w)
    if creds is False:
        print('enter credentials')
        username = input('Username: ')
        password = getpass.getpass()
    else:
        username, password = creds
    result = api.poweroff_cloudkey(w, username, password)
    if result is True:
        print('Sent power off command')
    else:
        print('Failed to send power off command')
print('Program complete.')