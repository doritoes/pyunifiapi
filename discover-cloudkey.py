#!/usr/bin/env python3
""" discover Ubiquiti Unifi CloudKey devices
    and return information about them
"""
from pycloudkeyapi import pyCloudkeyApi
print('Discovering Ubiquiti Unifi CloudKeys...')
api = pyCloudkeyApi()
keys = api.discover()
for key in keys:
    key_ip = key['ip']
    print('\nFound Cloudkey:')
    print(' [+] IP: ' + key_ip)
    print(' [+] Version: ' + key['version'])
    #print(' [+] Location: ' + key['path']) # debug
    #print(' [+] Location readable: {}'.format(key['path_readable'])) # debug
    result = api.test_cloudkey_default_ssh_credentials(key_ip)
    print(' [+] Default credentials: {}'.format(api.test_cloudkey_default_ssh_credentials(key_ip)))
    creds = api.get_creds(key_ip)
    if creds is not False:
        u, p = creds
        print('     [-] Username: {}\n     [-] Password: {}'.format(u, p))
        model, version, firmware = api.test_cloudkey_firmware_default(key_ip, u, p)
        print(' [+] Model: {}'.format(model))
        print(' [+] Firmware version: {}'.format(version))
        if firmware is not False:
            print('     {}'.format(firmware.strip()))
        if model is not False:
            latest_version, latest_full = api.get_latest_firmware_info(model)
        else:
            latest_version = False
            latest_full = False
        print(' [+] Latest firmware available: {}\n     {}'.format(latest_version, latest_full))
        if api.check_new_device_upgrade(key_ip, u, p):
            print(' [!] Upgrade available')
print('Discovery complete')