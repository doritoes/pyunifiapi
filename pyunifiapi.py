#!/usr/bin/env python3
""" prototype basic python wrapper to unifi api

    Credit to https://github.com/Art-of-WiFi/UniFi-API-client/blob/master/src/Client.php
    Security analysis https://medium.com/tenable-techblog/exploring-the-ubiquiti-unifi-cloud-key-gen2-plus-f5b0f7ca688
    Documentation and CloudKey class: https://gitlab.com/doritoes/pyunifiapi

    See also: https://help.ui.com/hc/en-us/articles/204976094-UniFi-Communication-Protocol-Between-Controller-and-UAP

    TODO
          add __str__ and/or __repr__ functions
          disable DPI which is on by default
               api/s/default/get/setting (GET) unifi_idp_enabled: true, false

          add a set-default ssh command for APs somewhere...

          test:
            downgrade firmware
            upgrade firmware
            start_rolling_upgrade
            cancel_rolling_upgrade
            force provision
            disable/enable device

          enhance:
            improve create wlan
            self-documentation

          more functions.....
            get port statuses for a device
            move device to other site: move-device
            modify switch port - not documented!!!!
            create wlan group
          endpoints to do:
            stat/dashboard
            stat/rogueap
            stat/voucher
            stat/payment
            stat/portforward
            stat/dpi
            stat/current-channel
            stat/ccode
            stat/spectrum-scan
            stat/report
            stat/ips/event
            stat/session
            stat/authorization
            stat/alluser
            stat/guest
            list/user
            list/portforward
            list/dynamicdns
            list/portconf
            list/extension
            list/alarm
            rest/firewallgroup
            rest/firewallrule
            rest/routing
            rest/tag
            rest/rogueunknown
            rest/setting/country/
            rest/setting/locale
            rest/setting/snmp
            rest/setting/mgmt
            rest/setting/ntp
            rest/setting/connectivity
            rest/hotspotop
            rest/device
            rest/networkconf
            rest/radiusprofile
            rest/account

          #'device-update': 'rest/device/{_id}

          add for APs:
            default: true/false
            adopted: false/true
            upgrade_state: 0, etc.
            adopt_status: 0, etc.
            "disabled" appears only when the AP is disabled
            "adoptable_where_upgrade"
            "upgradeable" appears when there is firmware found for it
            "required_version"
          Upgrade states (state 4, have seen upgrade states 2, 5)

          turn off l2 discover of controller
"""

class pyUnifiAPI:
    """ provide API to Unifi
    """
    __version__ = '0.5' # unstable
    __documentation__ = 'https://ubntwiki.com/products/software/unifi-controller/api'
    CONTROLLER_ENDPOINTS = {
        'admins': 'api/stat/admin',
        'login': 'api/login',
        'logout': 'api/logout',
        'self': 'api/self',
        'sites': 'api/self/sites',
        'sitestats': 'api/stat/sites',
        'status': 'status'
    }

    SITE_ENDPOINTS = { # TODO finish the rest
        'alarms-all': 'rest/alarm',
        'alarms-recent': 'stat/alarm',
        'analytics': 'cmd/analytics',
        'channels': 'stat/current-channel',
        'client': 'stat/user',
        'clients-active': 'stat/sta',
        'clients-all': 'rest/user',
        'country': 'stat/ccode',
        'device-disable': 'rest/device',
        'device-update': 'upd/device',
        'devices-all': 'stat/device',
        'devices-basic': 'stat/device-basic',
        'events-all': 'rest/event',
        'events-recent': 'stat/event',
        'health': 'stat/health',
        'provider-capabilities': 'set/setting/provider_capabilities',
        'self-site': 'self',
        'sysinfo': 'stat/sysinfo',
        'upgrade-progress': 'stat/updateprogress',
        'usergroups': 'list/usergroup',
        'usergroup': 'rest/usergroup',
        'wlan-add': 'add/wlanconf',
        'wlangroups': 'list/wlangroup',
        'wlans': 'rest/wlanconf'
    }

    SITE_ENDPOINTS_V2 = {
        'apgroups': 'apgroups'
    }

    COMMANDS_ENDPOINTS = [
        'backup',
        'devmgr',
        'evtmgt',
        'sitemgr',
        'stamgr',
        'stat',
        'system'
    ]
    HTTP_STATUS = {
        200: 'Successful',
        400: 'Bad Request', # usually problem with code
        401: 'Unauthorized' # usually token expired
    }
    DEVICE_STATE = {
        0: 'Disconnected',
        1: 'OK', # connected even if disabled
        2: 'Pending Adoption', # also Pending Adoption (wireless)
        4: 'Updating (Writing)', # also updating (failed)
        5: 'Provisioning',
        6: 'Heartbeat Missed',
        7: 'Adopting',
        10: 'Restarting',
        11: 'Connected (Limited)??' # connected but STUN service down
    }
    UPGRADE_STATE = {
        2: 'Failed', # state 4 upgrade state 2, Updating, Failed
        5: 'Writing' # state 5 upgrade state 5, Writing
    }
    # unknown codes: RF Scanning, Upgrading, Managed by Other, Isolated, Pending Adoption (Update Required), Connected (Limited), Connected (Disabled), Connected (100FDX), Connected (Wireless)
    # TODO What does adoption failed look like?
    DEVICE_TYPES = { # updated 10/4/2020
        # on the controller find the data in javascript files
        # find /usr/lib/unifi -type f -name \*js -exec grep -l U2L48 {} \;
        # or /usr/lib/unifi/dl/firmware/bundles.json
        'BZ2': {'description': 'Unifi AP',
                'sku': 'UAP',
                'type': 'uap'},
        'BZ2LR': {'description': 'UniFi AP-LR',
                  'sku': 'UAP-LR',
                  'type': 'uap'},
        'U2HSR': {'description': 'UniFi AP-Outdoor+',
                  'sku': 'UAP-Outdoor+',
                  'type': 'uap'},
        'U2IW': {'description': 'UniFi AP-In Wall',
                 'sku': '',
                 'type': 'uap'},
        'U2L48': {'description': 'UniFi AP-LR',
                  'sku': 'UAP-LR',
                  'type': 'uap'},
        'U2Lv2': {'description': 'UniFi AP-LR v2',
                  'sku': 'UAL-LR v2',
                  'type': 'uap'},
        'U2M': {'description': 'UniFi AP-Mini',
                'sku': 'UAP-MINI',
                'type': 'uap'},
        'U2O': {'description': 'UniFi AP-Outdoor',
                'sku': 'UAP-Outdoor',
                'type': 'uap'},
        'U2S48': {'description': 'UniFi AP',
                  'sku': '',
                  'type': 'uap'},
        'U2Sv2': {'description': 'UniFi AP v2',
                  'sku': '',
                  'type': 'uap'},
        'U5O': {'description': 'UniFi AP-Outdoor 5G',
                'sku': '',
                'type': 'uap'},
        'U7E': {'description': 'UniFi AP-AC UAP-AC',
                'sku': 'UAP-AC',
                'type': 'uap'},
        'U7EDU': {'description': 'UniFi AP-AC-EDU',
                  'sku': 'UAP-AC-EDU',
                  'type': 'uap'},
        'U7Ev2': {'description': 'UniFi AP-AC v2',
                  'sku': '',
                  'type': 'uap'},
        'U7HD': {'description': 'UniFi AP-HD',
                 'sku': '',
                 'type': 'uap'},
        'U7SHD': {'description': 'UniFi AP-SHD',
                  'sku': '',
                  'type': 'uap'},
        'U7NHD': {'description': 'UniFi AP-nanoHD',
                  'sku': 'UAP-nanoHD',
                  'type': 'uap'},
        'UAE6': {'description': 'Unifi AP-Extender',
                 'sku': '',
                 'type': 'uap'},
        'UFLHD': {'description': 'UniFi AP-Flex-HD',
                  'sku': 'UAP-FlexHD',
                  'type': 'uap'},
        'UHDIW': {'description': 'UniFi AP-HD-In Wall',
                  'sku': 'UAP-IW-HD',
                  'type': 'uap'},
        'UCXG': {'description': 'UniFi AP-XG',
                 'sku': 'UAP-XG',
                 'type': 'uap'},
        'UXSDM': {'description': 'UniFi AP-BaseStationXG',
                  'sku': 'UBW-XG',
                  'type': 'uap'},
        'UXBSDM': {'description': 'UniFi AP-BaseStationXG-Black',
                   'sku': 'UBW-XG-BK',
                   'type': 'uap'},
        'UCMSH': {'description': 'UniFi AP-MeshXG',
                  'sku': '',
                  'type': 'uap'},
        'U7IW': {'description': 'UniFi AP-AC-In Wall',
                 'sku': 'UAP-AC-IW',
                 'type': 'uap'},
        'U7IWP': {'description': 'UniFi AP-AC-In Wall Pro',
                  'sku': 'UAP-AC-IW-PRO',
                  'type': 'uap'},
        'U7MP': {'description': 'UniFi AP-AC-Mesh-Pro',
                 'sku': '',
                 'type': 'uap'},
        'U7LR': {'description': 'UniFi AP-AC-LR',
                 'sku': '',
                 'type': 'uap'},
        'U7LT': {'description': 'UniFi AP-AC-Lite',
                 'sku': '',
                 'type': 'uap'},
        'U7O': {'description': 'UniFi AP-AC Outdoor',
                'sku': '',
                'type': 'uap'},
        'U7P': {'description': 'UniFi AP-Pro',
                'sku': '',
                'type': 'uap'},
        'U7MSH': {'description': 'UniFi AP-AC-Mesh',
                  'sku': '',
                  'type': 'uap'},
        'U7PG2': {'description': 'UniFi AP-AC-Pro',
                  'sku': '',
                  'type': 'uap'},
        'UAL6' : {'description': 'U6-Lite',
                  'sku': '',
                  'type': 'uap'},
        'UALR6' : {'description': 'U6-LR',
                   'sku': '',
                   'type': 'uap'},
        'UAM6' : {'description': 'U6-Mesh',
                  'sku': '',
                  'type': 'uap'},
        'UAP6' : {'description': 'U6-Pro',
                  'sku': '',
                  'type': 'uap'},
        'UAIW6' : {'description': 'U6-IW',
                   'sku': '',
                   'type': 'uap'},
        'p2N': {'description': 'PicoStation M2',
                'sku': '',
                'type': 'uap'},
        'UDMB': {'description': 'UniFi AP-BeaconHD',
                 'sku': 'UDM-B',
                 'type': 'uap'},
        'USF5P': {'description': 'UniFi Switch Flex 4 POE',
                  'sku': '',
                  'type': 'usw'},
        'US8': {'description': 'UniFi Switch 8',
                'sku': '',
                'type': 'usw'},
        'US8P60': {'description': 'UniFi Switch 8 POE-60W',
                   'sku': '',
                   'type': 'usw'},
        'US8P150': {'description': 'UniFi Switch 8 POE-150W',
                    'sku': '',
                    'type': 'usw'},
        'S28150': {'description': 'UniFi Switch 8 AT-150W',
                   'sku': '',
                   'type': 'usw'},
        'USC8': {'description': 'UniFi Switch 8',
                 'sku': '',
                 'type': 'usw'},
        'USC8P60': {'description': 'UniFi Switch 8 POE-60W',
                    'sku': '',
                    'type': 'usw'},
        'USC8P150': {'description': 'UniFi Switch 8 POE-150W',
                     'sku': '',
                     'type': 'usw'},
        'US16P150': {'description': 'UniFi Switch 16 POE-150W',
                     'sku': '',
                     'type': 'usw'},
        'S216150': {'description': 'UniFi Switch 16 AT-150W',
                    'sku': '',
                    'type': 'usw'},
        'US24': {'description': 'UniFi Switch 24',
                 'sku': '',
                 'type': 'usw'},
        'US24PRO': {'description': 'UniFi Switch PRO 24 POE',
                    'sku': '',
                    'type': 'usw'},
        'US24PRO2': {'description': 'UniFi Switch PRO 24',
                     'sku': '',
                     'type': 'usw'},
        'US24P250': {'description': 'UniFi Switch 24 POE-250W',
                     'sku': '',
                     'type': 'usw'},
        'US24PL2': {'description': 'UniFi Switch 24 L2 POE',
                    'sku': '',
                    'type': 'usw'},
        'US24P500': {'description': 'UniFi Switch 24 POE-500W',
                     'sku': '',
                     'type': 'usw'},
        'S224250': {'description': 'UniFi Switch 24 AT-250W',
                    'sku': '',
                    'type': 'usw'},
        'S224500': {'description': 'UniFi Switch 24 AT-500W',
                    'sku': '',
                    'type': 'usw'},
        'US48': {'description': 'UniFi Switch 48',
                 'sku': '',
                 'type': 'usw'},
        'US48PRO': {'description': 'UniFi Switch PRO 48 POE',
                    'sku': '',
                    'type': 'usw'},
        'US48PRO2': {'description': 'UniFi Switch PRO 48',
                     'sku': '',
                     'type': 'usw'},
        'US48P500': {'description': 'UniFi Switch 48 POE-500W',
                     'sku': '',
                     'type': 'usw'},
        'US48PL2': {'description': 'UniFi Switch 48 L2 POE',
                    'sku': '',
                    'type': 'usw'},
        'US48P750': {'description': 'UniFi Switch 48 POW-750W',
                     'sku': '',
                     'type': 'usw'},
        'US624P': {'description': 'UniFi6 Switch 24',
                   'sku': '',
                   'type': 'usw'},
        'S248500': {'description': 'UniFi Switch 48 AT-500W',
                    'sku': '',
                    'type': 'usw'},
        'S248750': {'description': 'UniFi Switch 48 AT-750W',
                    'sku': '',
                    'type': 'usw'},
        'US6XG150': {'description': 'UniFi Switch XG 6 POE',
                     'sku': '',
                     'type': 'usw'},
        'USL8A': {'description': 'USW-AGGREGATION',
                  'sku': 'USW-AGGREGATION',
                  'type': 'usw'},
        'USAGGPRO': {'description': 'Unifi Switch Aggregation Pro',
                     'sku': '',
                     'type': 'usw'},
        'USL8MP': {'description': 'UniFi Switch Mission Critical',
                   'sku': '',
                   'type': 'usw'},
        'USMINI': {'description': 'UniFi Switch Flex Mini',
                   'sku': '',
                   'type': 'usw'},
        'USXG': {'description': 'UniFi Switch 16XG',
                 'sku': '',
                 'type': 'usw'},
        'USC8P450': {'description': 'UniFi Switch Industrial 8 POE-450W',
                     'sku': '',
                     'type': 'usw'},
        'UDC48X6': {'description': 'UniFi Switch Leaf',
                    'sku': '',
                    'type': 'usw'},
        'USL8LP': {'description': 'UniFi Switch Lite 8 POE',
                   'sku': '',
                   'type': 'usw'},
        'USL16P': {'description': 'UniFi Switch 16 POE',
                   'sku': '',
                   'type': 'usw'},
        'USL16LP': {'description': 'UniFi Switch Lite 16 POE',
                    'sku': '',
                    'type': 'usw'},
        'USL24': {'description': 'UniFi Switch 24',
                  'sku': '',
                  'type': 'usw'},
        'USL48': {'description': 'UniFi Switch 48',
                  'sku': '',
                  'type': 'usw'},
        'USL24P': {'description': 'UniFi Switch 24 POE',
                   'sku': '',
                   'type': ''},
        'USL48P': {'description': 'UniFi Switch 48 POE',
                   'sku': '',
                   'type': ''},
        'UGW3': {'description': 'UniFi Security Gateway 3P',
                 'sku': '',
                 'type': 'ugw'},
        'UGW4': {'description': 'UniFi Security Gateway 4P',
                 'sku': '',
                 'type': 'ugw'},
        'UGWHD4': {'description': 'UniFi Security Gateway HD',
                   'sku': '',
                   'type': 'ugw'},
        'UGWXG': {'description': 'UniFi Security Gateway XG-8',
                  'sku': '',
                  'type': 'ugw'},
        'UXGPRO': {'description': 'UniFi NeXt-Gen Gateway PRO',
                   'sku': '',
                   'type': 'ugw'},
        'UP4': {'description': 'UniFi Phone-X',
                'sku': '',
                'type': 'uph'},
        'UP5': {'description': 'UniFi Phone',
                'sku': '',
                'type': 'uph'},
        'UP5t': {'description': 'UniFi Phone-Pro',
                 'sku': '',
                 'type': 'uph'},
        'UP7': {'description': 'UniFi Phone-Executive',
                'sku': '',
                'type': 'uph'},
        'UP5c': {'description': 'UniFi Phone',
                 'sku': '',
                 'type': 'uph'},
        'UP5tc': {'description': 'UniFi Phone-Pro',
                  'sku': '',
                  'type': 'uph'},
        'UP7c': {'description': 'UniFi Phone-Executive',
                 'sku': '',
                 'type': 'uph'},
        'UCK': {'description': 'UniFi Cloud Key',
                'sku': '',
                'type': 'uck'},
        'UCK-v2': {'description': 'UniFi Cloud Key v2',
                   'sku': '',
                   'type': 'uck'},
        'UCK-v3': {'description': 'UniFi Cloud Key v3',
                   'sku': '',
                   'type': 'uck'},
        'UCKG2': {'description': 'UniFi Cloud Key Gen2',
                  'sku': '',
                  'type': 'uck'},
        'UCKP': {'description': 'UniFi Cloud Key Gen2 Plus',
                 'sku': '',
                 'type': 'uck'},
        'UASXG': {'description': 'UniFi Application Server XG',
                  'sku': '',
                  'type': 'uas'},
        'ULTE': {'description': 'UniFi LTE',
                 'sku': '',
                 'type': 'uap'},
        'ULTEPEU': {'descripton': 'UniFi LTE Pro EU',
                    'sku': '',
                    'type': 'uap'},
        'ULTEPUS': {'descripton': 'UniFi LTE Pro US',
                    'sku': '',
                    'type': 'uap'},
        'UP1': {'description': 'UniFi Smart Plug',
                'sku': '',
                'type': 'uap'},
        'UP6': {'description': 'UniFi Smart Power 6-Port Power Strip',
                'sku': '',
                'type': 'uap'},
        'USPPDUP': {'description': 'UniFi Smart Power Power Distribution Unit Pro',
                    'sku': '',
                    'type': 'usw'},
        'UBB': {'description': 'UniFi Building Bridge',
                'sku': '',
                'type': 'ubb'},
        'USPRPS': {'description': 'UniFi Smart Power - Redundant Power System',
                   'sku': '',
                   'type': 'usw'},
        'UDM': {'description': 'UniFi Dream Machine',
                'sku': '',
                'type': 'udm'},
        'UDMSE': {'description': 'UniFi Dream Machine SE',
                  'sku': '',
                  'type': 'udm'},
        'UDMPRO': {'description': 'UniFi Dream Machine Pro',
                   'sku': '',
                   'type': 'udm'}
    }

    # functions
    @staticmethod
    def valid_ssid(ssid_string):
        """ test if a string is a valid SSID
            WARNING Not entirely pleased with this regex
        """
        import re
        p = re.compile(r'^[\u0020-\u0073\u00a0-\u00ff]{1,32}$')
        if p.match(ssid_string):
            return True
        return False

    @staticmethod
    def valid_passphrase(passphrase_string):
        """ test if a string is a valid SSID
            WARNING Not entirely pleased with this regex
            NOTE per standard assign value of null when
                 security is set to 'open'
        """
        import re
        p = re.compile(r'^[\u0020-\u007e\u00a0-\u00ff]{8,63}$')
        if p.match(passphrase_string):
            return True
        return False

    @staticmethod
    def _read_api(path, params=None, verify=None):
        """ use _read_api to read a JSON resource using
            a simple GET
        """
        import json
        import requests
        s = requests.session()
        s.headers = {'content-type': 'application/json'}
        requests.packages.urllib3.disable_warnings()
        if verify is False:
            s.verify = False
        elif verify is None:
            pass
        else:
            s.verify = verify
        if params is None:
            r = s.get(path)
        else:
            try:
                r = s.get(path, params=params)
            except requests.exceptions.RequestException:
                return False
        if r.status_code != 200:
            return False
        try:
            j = r.json()
        except json.JSONDecodeError:
            return False
        return j

    # internal class methods
    def __init__(self, user, password, server, port=8443, verify=None):
        """ __init__ is the init method """
        import requests
        if verify is None or verify is False:
            requests.packages.urllib3.disable_warnings()
        self.user = user
        self.password = password
        self.url = server
        self.port = port
        self.session = requests.session()
        self.session.headers = {'content-type': 'application/json'}
        if verify is None:
            requests.packages.urllib3.disable_warnings()
            self.session.verify = False
        else:
            self.session.verify = verify
        result = self._login()
        if result is False:
            import sys
            print('Failed login. Check your settings and try again.')
            sys.exit()

    def _build_url(self, endpoint, site=None):
        """ use _build_url build a URL for the requested API endpoint
        """
        if endpoint in self.SITE_ENDPOINTS:
            if site is None:
                return False
            return 'https://{}:{}/{}/{}/{}'.format(self.url,
                                                   self.port,
                                                   'api/s',
                                                   site,
                                                   self.SITE_ENDPOINTS[endpoint])
        if endpoint in self.CONTROLLER_ENDPOINTS:
            if site is not None:
                return False
            return 'https://{}:{}/{}'.format(self.url,
                                             self.port,
                                             self.CONTROLLER_ENDPOINTS[endpoint])
        if endpoint in self.COMMANDS_ENDPOINTS:
            if site is None:
                return False
            return 'https://{}:{}/{}/{}/cmd/{}'.format(self.url,
                                                       self.port,
                                                       'api/s',
                                                       site,
                                                       endpoint)
        if endpoint in self.SITE_ENDPOINTS_V2:
            if site is None:
                return False
            return 'https://{}:{}/{}/{}/{}'.format(self.url,
                                                   self.port,
                                                   'v2/api/site',
                                                   site,
                                                   self.SITE_ENDPOINTS_V2[endpoint])
        return False

    def _login(self):
        """ use _login to login to the unifi controller/api
        """
        posturl = self._build_url('login')
        if posturl is False:
            raise ValueError
        data = {'username': self.user,
                'password': self.password}
        r = self.session.post(posturl, json=data)
        if r.status_code == 200:
            return True
        # status_code 400 for bad password
        return False

    def _get_user_group_id(self, site, name):
        """ get user group id from the name
        """
        user_groups = self.get_user_groups(site)
        if user_groups is False:
            return False
        for ug in user_groups:
            if ug['name'] == name:
                return ug['_id']
        return False

    def _get_wlan_id(self, site, name):
        """ get wlan id from the name
        """
        wlans = self.get_wlans(site)
        if wlans is False:
            return False
        for w in wlans:
            if w['name'] == name:
                return w['_id']
        return False

    def _create_wlan(self, site, wlanname, wlanpassword, wlangroupid,
                     usergroupid, apgroupid, wlansecurity, wpamode,
                     isenabled=True, isguest=False, hidessid=False,
                     wlanvlan=None, uapsdenabled=False,
                     schedule_enabled=False, schedule=None):
        """ TO DO improve this function to support all options
        """
        posturl = self._build_url('wlan-add', site)
        data = {'name': wlanname,
                'x_passphrase': wlanpassword,
                'usergroup_id': usergroupid,
                'wlangroup_id': wlangroupid}
        data['enabled'] = isenabled
        data['is_guest'] = isguest
        data['hide_ssid'] = hidessid
        data['ap_group_ids'] = [apgroupid]
        if wlansecurity not in ['open', 'wep', 'wpapsk', 'wpaeap']:
            return False
        if wlansecurity == 'wpapsk':
            data['security'] = wlansecurity
            data['wpa_mode'] = wpamode
            data['wpa_enc'] = 'ccmp'
        if wlanvlan is not None:
            data['vlan_enabled'] = True
            data['vlan'] = wlanvlan
        if uapsdenabled:
            data['uapsd_enabled'] = uapsdenabled
        if schedule_enabled:
            data['schedule_enabled'] = True
            data['schedule'] = schedule
        r = self.session.post(posturl, json=data)
        if r.status_code == 200:
            return True
        print('DEBUG: {}'.format(r.status_code)) # debug
        print(r.text) # debug
        return False

    def _set_wlan_enabled(self, site, name, state):
        """ change the enabled status of a wlan
        """
        puturl = self._build_url('wlans', site)
        wlanid = self._get_wlan_id(site, name)
        if wlanid is False:
            return False
        puturl += '/' + wlanid
        if state is True:
            data = {'enabled': True}
        elif state is False:
            data = {'enabled': False}
        else:
            return False
        r = self.session.put(puturl, json=data)
        if r.status_code == 200:
            return True
        return False

    # public class methods
    # dedicated test functions
    def test_build_url(self, endpoint, site=None):
        """ test _build_url
        """
        return self._build_url(endpoint, site)

    def test_get_wlan_id(self, site, name):
        """ test _get_wlan_id
        """
        return self._get_wlan_id(site, name)

    # login/logout
    def get_logged_in_user(self):
        """ use get_logged_in_user to return username
        """
        geturl = self._build_url('self')
        r = self.session.get(geturl)
        if r.status_code == 200:
            j = r.json()
            for i in j['data']:
                if 'name' in i:
                    return i['name']
        return False

    def logout(self):
        """ use logout to close session
        """
        posturl = self._build_url('logout')
        r = self.session.post(posturl)
        self.session.cookies.clear()
        if r.status_code == 200:
            return True
        return False

    def renew_login(self):
        """ use renew_login to reopen a session
        """
        self.session.cookies.clear()
        return self._login()

    # sites
    def get_sites(self):
        """ use get_sites to get a list of sites
        """
        geturl = self._build_url('sites')
        r = self.session.get(geturl)
        if r.status_code == 200:
            j = r.json()
            sites = []
            for i in j['data']:
                if 'name' in i:
                    sites.append(i['name'])
            return sites
        return False

    def get_site_by_id(self, site_id):
        """ use get_site_by_id to get site details
            for a site id
        """
        geturl = self._build_url('sites')
        r = self.session.get(geturl)
        if r.status_code == 200:
            j = r.json()
            for i in j['data']:
                if 'name' in i and i['name'] == site_id:
                    return i
        return False

    def get_site_by_description(self, description):
        """ use get_site_by_description to return the
            site id for the site with the matching
            description
        """
        geturl = self._build_url('sites')
        r = self.session.get(geturl)
        if r.status_code == 200:
            j = r.json()
            for i in j['data']:
                if 'desc' in i and i['desc'] == description:
                    return i['name']
        return False

    def add_site(self, new_site):
        """ use add_site to add a site
        """
        data = {'cmd': 'add-site',
                'desc': new_site}
        sites = self.get_sites()
        site_id = sites[0]
        posturl = self._build_url('sitemgr', site_id)
        r = self.session.post(posturl, json=data)
        if r.status_code == 200:
            return True
        return False

    def rename_site(self, site_id, new_description):
        """ use rename_site to rename a site
            changes description of the site by id
        """
        data = {'cmd': 'update-site',
                'desc': new_description}
        posturl = self._build_url('sitemgr', site_id)
        r = self.session.post(posturl, json=data)
        if r.status_code == 200:
            return True
        return False

    def remove_site(self, site_id):
        """ use delete_site to delete a site
            requires site_id (not description)
            and the internal _id value
        """
        site_data = self.get_site_by_id(site_id)
        raw_id = site_data['_id']
        data = {'cmd': 'delete-site',
                'site': raw_id}
        posturl = self._build_url('sitemgr', site_id)
        r = self.session.post(posturl, json=data)
        if r.status_code == 200:
            return True
        return False

    def set_provider_capabilities(self, site_id, download, upload):
        """ use set_provider_capabilities to set the site
            ISP capabilties
            download and upload in KBps
            100Mbps = 100000
            10Mbps = 10000
        """
        data = {'key': 'provider_capabilities',
                'site_id': site_id,
                'download': download,
                'upload': upload}
        posturl = self._build_url('provider-capabilities', site_id)
        r = self.session.post(posturl, json=data)
        if r.status_code == 200:
            return True
        return False

    # devices
    def get_devices_quick(self, site):
        """ use get_devices_quick to get list of connected Unifi devices
        """
        geturl = self._build_url('devices-basic', site)
        r = self.session.get(geturl)
        if r.status_code == 200:
            content = r.json()
            devices = {}
            for i in content['data']:
                if 'name' in i:
                    device_id = i['name']
                else:
                    device_id = "{}-Unnamed".format(i['mac'])
                devices[device_id] = {}
                for j in ['type', 'state', 'adopted', 'disabled', 'model']:
                    devices[device_id][j] = i[j]
                devices[device_id]['description'] = self.DEVICE_TYPES[i['model']]['description']
            return devices
        return False

    def get_devices(self, site, device_type=None):
        """ use get_devices to get detailed list of all devices on site
            optionally provide a device_type to filter by
        """
        devtypes = ['uap', 'usw', 'ugw', 'udm', 'uph',
                    'uck', 'uas']
        if device_type is not None and device_type.lower() not in devtypes:
            return False
        geturl = self._build_url('devices-all', site)
        r = self.session.get(geturl)
        if r.status_code == 200:
            content = r.json()
            if device_type is None:
                return content['data']
            filtered_list = []
            for d in content['data']:
                if d['type'] == device_type.lower():
                    filtered_list.append(d)
            return filtered_list
        return False

    def get_device_by_mac(self, site, mac):
        """ use get_device_by_mac to get device details
            by its mac address
        """
        import requests
        geturl = self._build_url('devices-all', site)
        geturl += '/' + mac.lower().strip()
        try:
            r = self.session.get(geturl)
        except requests.exceptions.RequestException:
            return False
        if r.status_code == 200:
            content = r.json()
            return content['data']
        return False

    def get_device_by_name(self, site, name):
        """ use get_device_by_name to get device details
            by its name
        """
        r = []
        d = self.get_devices(site)
        for i in d:
            if 'name' in i and i['name'] == name:
                r.append(i)
        if r:
            return r
        return False

    def get_device_overview_by_name(self, site, name):
        """ use get_device_overview_by_name to get the most
            useful detail for a device
        """
        d = self.get_device_by_name(site, name)
        r = {}
        for i in ['adopted', 'model', 'version',
                  'name', 'mac', 'uptime', 'upgradable',
                  'led_override']:
            if i in d[0]:
                r[i] = d[0][i]
            else:
                if i not in ['led_override']:
                    r[i] = False
        r['description'] = self.DEVICE_TYPES[d[0]['model']]
        r['radios'] = []
        if 'radio_table_stats' in d[0]: # for type uap, this will match
            for radio in d[0]['radio_table_stats']:
                data = {}
                for j in ['radio', 'channel', 'state']:
                    data[j] = radio[j]
                if radio['radio'] == 'ng':
                    data['band'] = '2.4'
                if radio['radio'] == 'na':
                    data['band'] = '5'
                data['clients'] = radio['cu_total']
                r['radios'].append(data)
        return r

    def get_devices_brief_by_type(self, site, devtype):
        """ use get_devices_brief_by_type to get a
            quick list by type: uap, usw, ugw, udm,
                                uph, uck, uas
        """
        devtypes = ['uap', 'usw', 'ugw', 'udm', 'uph',
                    'uck', 'uas']
        if devtype.lower() in devtypes:
            selected_type = devtype.lower()
        else:
            if devtype.lower() in ['switch', 'sw']:
                selected_type = 'usw'
            elif devtype.lower() in ['usg', 'gateway', 'firewall', 'gw', 'fw']:
                selected_type = 'ugw'
            elif devtype.lower() in ['ap', 'wifi']:
                selected_type = 'uap'
            elif devtype.lower() in ['dreammachine']:
                selected_type = 'udm'
            elif devtype.lower() in ['phone']:
                selected_type = 'uph'
            elif devtype.lower() in ['cloudkey', 'controller', 'ck']:
                selected_type = 'uck'
            elif devtype.lower() in ['server']:
                selected_type = 'uas'
            else:
                return False
        r = []
        d = self.get_devices(site)
        for i in d:
            if i['type'] == selected_type:
                r.append(i)
        if r:
            return r
        return False

    def force_provision_device(self, site, mac_address):
        """ force provision a deivce by its MAC address
        """
        data = {'cmd': 'force-provision',
                'mac': mac_address.lower()}
        posturl = self._build_url('devmgr', site)
        r = self.session.post(posturl, json=data)
        if r.status_code == 200:
            return True
        return False

    def upgrade_device(self, site, mac_address, firmware_url=None):
        """ upgrade firmware on a device, default to latest version,
            or to image at specified URL
        """
        data = {'mac': mac_address.lower()}
        posturl = self._build_url('devmgr', site)
        posturl += '/upgrade'
        if firmware_url is None: # latest firmware known to controller
            r = self.session.post(posturl, json=data)
        else:
            data['url'] = firmware_url
            posturl += '-external'
        r = self.session.post(posturl, json=data)
        if r.status_code == 200:
            result = r.json()
            if result['meta']['rc'] == 'ok':
                return True
        if r.status_code == 400:
            result = r.json()
            if result['meta']['rc'] == 'error':
                if result['meta']['msg'] == 'api.err.UpgradeInProgress':
                    return True
                print('Error: {}'.format(result['meta']['msg']))
        return False

    def get_ap_groups(self, site):
        """ get access point groups, need to provide an groud ip
            when creating a WLAN)
        """
        geturl = self._build_url('apgroups', site)
        r = self.session.get(geturl)
        if r.status_code == 200:
            return r.json()
        return False

    def start_rolling_upgrade(self, site):
        """ start rolling firmware upgrade
        """
        data = {'cmd': 'set-rollupgrade'}
        posturl = self._build_url('devmgr', site)
        r = self.session.post(posturl, json=data)
        if r.status_code == 200:
            return True
        return False

    def cancel_rolling_upgrade(self, site):
        """ cancel rolling firmware upgrade
        """
        data = {'cmd': 'unset-rollupgrade'}
        posturl = self._build_url('devmgr', site)
        r = self.session.post(posturl, json=data)
        if r.status_code == 200:
            return True
        return False

    def restart_device(self, site, mac_address, restart_type='soft'):
        """ restart a device
            soft can be used for all devices: requests a plan
            restart of the device

            hard is for PoE switches: restarts the devices and
            also power cycles all the PoE ports
        """
        data = {'cmd': 'restart', 'mac': mac_address.lower()}
        if restart_type.lower() not in ['soft', 'hard']:
            return False
        data['type'] = restart_type.lower()
        posturl = self._build_url('devmgr', site)
        r = self.session.post(posturl, json=data)
        if r.status_code == 200:
            return True
        return False

    def adopt_device(self, site, mac):
        """ adopt a device
            note that the hostname will be set to UBNT
            which means not set
        """
        data = {'cmd': 'adopt', 'mac': mac.lower()}
        posturl = self._build_url('devmgr', site)
        r = self.session.post(posturl, json=data)
        if r.status_code == 200:
            return True
        return False

    def rename_device(self, site, device_id, ap_name):
        """ rename a device
            tested successfully: AP, USG
        """
        data = {'name': ap_name}
        posturl = self._build_url('device-update', site)
        posturl += '/' + device_id
        r = self.session.post(posturl, json=data)
        if r.status_code == 200:
            return True
        return False

    def disable_device(self, site, ap_id, disabled=True):
        """ disable an access point (AP) by AP ID
            (device id). Disabled: True disables,
            and diabled: False enables.
        """
        if disabled not in [True, False]:
            return False
        data = {'disabled': disabled}
        posturl = self._build_url('device-disable', site)
        posturl += ap_id
        r = self.session.post(posturl, json=data)
        if r.status_code == 200:
            return True
        return False

    def remove_device_from_site(self, site, mac):
        """ remove device from a site
        """
        data = {'mac': mac,
                'cmd': 'delete-device'}
        posturl = self._build_url('sitemgr', site)
        r = self.session.post(posturl, json=data)
        if r.status_code == 200:
            return True
        return False

    def get_uplink(self, site, device_name):
        """ get_uplink returns information on a device's uplink
        """
        devices = self.get_device_by_name(site, device_name)
        if devices is False:
            return False
        device = devices[0]
        if 'uplink' not in device:
            return False
        uplink_data = {}
        uplink_data['up'] = device['uplink']['up'] # True or False
        if 'uplink_mac' in device['uplink']:
            uplink_data['mac'] = device['uplink']['uplink_mac'] # remote MAC
        else:
            uplink_data['mac'] = False
        if 'uplink_remote_port' in device['uplink']:
            uplink_data['port'] = device['uplink']['uplink_remote_port'] # remote port index
        else:
            uplink_data['port'] = False
        uplink_data['local_port'] = device['uplink']['num_port'] # local port index
        for i in ['up', # True or False
                  'type', # wire, ?wireless?
                  'speed', # 1000 or 100
                  'full_duplex', # True or False
                  'name']: # eth0, etc. local interface name
            uplink_data[i] = device['uplink'][i]
        return uplink_data

    # switches
    def power_cycle_switch_port(self, site, switch_mac, port_index):
        """ power cycle the PoE output of a port on Unifi switch
            the port must be providing power

            requires the main MAC of the switch
            port index matches port number (not zero index)

            use api.get_uplink to get the uplink info for
            a device by name
        """
        data = {'mac': switch_mac.lower(),
                'port_idx': int(port_index),
                'cmd': 'power-cycle'}
        posturl = self._build_url('devmgr', site)
        r = self.session.post(posturl, json=data)
        if r.status_code == 200:
            return True
        return False

    def get_ports(self, site, name):
        """ get list of ports and basic stats

            note that UAPs without a second port may
            not return any ports

            switches have addition values, we don't use all
               port_idx: integer 1-x
               is_uplink: True, False
               media: GE, ????
               port_poe: True, False
               poe_mode: auto, ???
               poe_class: "Unknown", "Class 0", "Class 4", ????
               poe_current: string float "0.00"
               poe_good: False, True
               poe_voltage: string float "0.00"
               autoneg: True, False
               sfp_found: True, False
               op_mode: switch, ????
               aggregated_by: False, True

            non-switches
               ifname
        """
        devices = self.get_device_by_name(site, name)
        if devices is False:
            return False
        device = devices[0]
        if 'port_table' not in device:
            return False
        port_data = {}
        for port in device['port_table']:
            port_data[port['name']] = {}
            for i in ['name', 'ifname', 'enable', 'up',
                      'speed', 'full_duplex', 'ip', 'netmask',
                      'port_idx', 'is_uplink', 'port_poe',
                      'poe_good', 'poe_class',
                      'autoneg', 'sfp_found', 'aggregated_by']:
                if i in port:
                    port_data[port['name']][i] = port[i]
        return port_data

    # clients
    def get_clients(self, site, active=True):
        """ use get_clients to get information network clients
            use active=False to get all, not just active clients
        """
        if active is True:
            geturl = self._build_url('clients-active', site)
        else:
            geturl = self._build_url('clients-all', site)
        r = self.session.get(geturl)
        if r.status_code == 200:
            content = r.json()
            return content['data']
        return False

    def get_client(self, site, mac):
        """ use get_client to get details of a single
            client by mac address
        """
        geturl = self._build_url('client', site)
        geturl += '/' + mac.lower().strip()
        r = self.session.get(geturl)
        if r.status_code == 200:
            content = r.json()
            return content['data']
        return False

    def get_clients_by_ap(self, site, ap_name=None):
        """ use get_clients by ap to return client MAC addresses
            by the AP name they are connected to
        """
        unifi_clients = self.get_clients(site)
        ap_list = {}
        matching_clients = []
        for unifi_client in unifi_clients:
            if 'ap_mac' in unifi_client:
                ap_mac = unifi_client['ap_mac']
                if ap_mac not in ap_list:
                    device_detail = self.get_device_by_mac(site, ap_mac)
                    ap_list[ap_mac] = device_detail[0]['name']
                if ap_name is None or ap_name == ap_list[ap_mac]:
                    matching_clients.append(unifi_client['mac'])
        return matching_clients

    def get_clients_by_ap_mac(self, site, ap_mac=None):
        """ use get_clients by ap to return client MAC addresses
            by the mac address of the AP they are connected to
        """
        unifi_clients = self.get_clients(site)
        matching_clients = []
        for unifi_client in unifi_clients:
            if 'ap_mac' in unifi_client:
                client_ap_mac = unifi_client['ap_mac']
                if client_ap_mac is None or client_ap_mac == ap_mac:
                    matching_clients.append(unifi_client['mac'])
        return matching_clients

    # users
    def get_user_groups(self, site):
        """ get list of user groups
        """
        geturl = self._build_url('usergroups', site)
        r = self.session.get(geturl)
        if r.status_code == 200:
            content = r.json()
            return content['data']
        return False

    def add_user_group(self, site, group_name, rate_down=None, rate_up=None):
        """ create a user group, just as a Guest Group with
            optional QoS bandwidth limitations in Kbps
        """
        posturl = self._build_url('usergroup', site)
        data = {'name': group_name}
        if rate_down is not None:
            data['qos_rate_max_down'] = rate_down
        if rate_up is not None:
            data['qos_rate_max_up'] = rate_up
        r = self.session.post(posturl, json=data)
        if r.status_code == 200:
            j = r.json()
            if j['meta']['rc'] == 'ok' and j['data']:
                return True
            return False
        return False

    def delete_user_group(self, site, group_name):
        """ remove a user group
        """
        user_groups = self.get_user_groups(site)
        user_group_id = None
        for ug in user_groups:
            if ug['name'] == group_name:
                user_group_id = ug['_id']
                break
        deleteurl = self._build_url('usergroup', site)
        deleteurl += '/{}'.format(user_group_id)
        r = self.session.delete(deleteurl)
        if r.status_code == 200:
            return True
        return False

    def modify_user_group(self, site, old_name, new_name, rate_down=None, rate_up=None):
        """ modify a user group
            setting rate to -1 disables the QoS
            accept false and zero, covert to -1
        """
        user_group_id = self._get_user_group_id(site, old_name)
        puturl = self._build_url('usergroup', site)
        puturl += '/{}'.format(user_group_id)
        data = {'name': new_name,
                'site_id': site,
                '_id': user_group_id}
        if rate_down is not None:
            if rate_down is False or rate_down <= 0:
                data['qos_rate_max_down'] = -1
            else:
                data['qos_rate_max_down'] = rate_down
        if rate_up is not None:
            if rate_up is False or rate_up <= 0:
                data['qos_rate_max_up'] = -1
            else:
                data['qos_rate_max_up'] = rate_up
        r = self.session.put(puturl, json=data)
        if r.status_code == 200:
            return True
        return False

    # wlans
    def get_wlans(self, site):
        """ use get_wlans to get the wireless lans
        """
        geturl = self._build_url('wlans', site)
        r = self.session.get(geturl)
        if r.status_code == 200:
            content = r.json()
            return content['data']
        return False

    def get_wlan_details(self, site, name):
        """ use get_wlan_details to get details for
            a wlan by name
        """
        wlans = self.get_wlans(site)
        for w in wlans:
            if w['name'] == name:
                return w
        return False

    def get_wlan_details_by_id(self, site, wlan_id):
        """ use get_wlan_details to get details of a
            wlan by id
        """
        geturl = self._build_url('wlans', site)
        geturl += '/' + wlan_id
        r = self.session.get(geturl)
        if r.status_code == 200:
            content = r.json()
            return content['data']
        return False

    def create_wlan_psk(self, site, wlanname, wlanpassword, wlangroupid, usergroupid, apgroupid, guestwlan=False):
        """ create a wlan with WPA2 PSK
        """
        if guestwlan is False:
            return self._create_wlan(site, wlanname, wlanpassword,
                                     wlangroupid, usergroupid,
                                     apgroupid,
                                     'wpapsk', 'wpa2')
        return self._create_wlan(site, wlanname, wlanpassword,
                                 wlangroupid, usergroupid,
                                 apgroupid,
                                 'wpapsk', 'wpa2',
                                 True, guestwlan)

    def remove_wlan(self, site, name):
        """ remove a wlan
        """
        deleteurl = self._build_url('wlans', site)
        deleteurl += '/' + self._get_wlan_id(site, name)
        r = self.session.delete(deleteurl)
        if r.status_code == 200:
            return True
        return False

    def enable_wlan(self, site, name):
        """ enable a wlan by site and name
        """
        return self._set_wlan_enabled(site, name, True)

    def disable_wlan(self, site, name):
        """ disable a wlan by site and name
        """
        return self._set_wlan_enabled(site, name, False)

    def set_wlan_user_group(self, site_id, wlan_name, user_group):
        """ set wlan user group
            to remove custom group, set to Default
        """
        wlan_id = self._get_wlan_id(site_id, wlan_name)
        user_group_id = self._get_user_group_id(site_id, user_group)
        puturl = self._build_url('wlans', site_id)
        puturl += '/{}'.format(wlan_id)
        data = {'site_id': site_id,
                'usergroup_id': user_group_id,
                '_id': wlan_id}
        r = self.session.put(puturl, json=data)
        if r.status_code == 200:
            return True
        return False

    def set_wlan_password(self, site, wlan_name, wlan_passphrase):
        """ set a wlan passphrase by wlan_id
        """
        if not self.valid_passphrase(wlan_passphrase):
            return False
        wlan_id = self._get_wlan_id(site, wlan_name)
        puturl = self._build_url('wlans', site)
        puturl += "/{}".format(wlan_id)
        data = {'x_passphrase': wlan_passphrase}
        r = self.session.put(puturl, json=data)
        if r.status_code == 200:
            return True
        return False

    def get_wlan_groups(self, site):
        """ get list of wlan groups
            Default, Off are created by default
        """
        geturl = self._build_url('wlangroups', site)
        r = self.session.get(geturl)
        if r.status_code == 200:
            content = r.json()
            return content['data']
        return False

    # unifi controller
    def reboot_cloudkey(self, site):
        """ reboot the cloudkey (API call does nothing on
            UniFi controllers *not* running on a UniFi
            CloudKey device
        """
        data = {'cmd': 'reboot'}
        posturl = self._build_url('devmgr', site)
        r = self.session.post(posturl, json=data)
        if r.status_code == 200:
            return True
        return False

    def get_alarms(self, site, allalarms=False):
        """ use get_alarms to recent alarms
            or all alarms if allalarms is True
        """
        if allalarms is False:
            geturl = self._build_url('alarms-recent', site)
        else:
            geturl = self._build_url('alarms-all', site)
        r = self.session.get(geturl)
        if r.status_code == 200:
            content = r.json()
            return content['data']
        return False

    def set_analytics(self, site, enabled):
        """ enable or more likely disable sending
            analytics to Ubiquiti

            setting up early while site is still
            "default" is advised

            TODO
            right now it sets OK, but get prompted
            first login to Unifi anyways
        """
        posturl = self._build_url('analytics', site)
        data = {}
        if posturl is False:
            raise ValueError
        if enabled is True:
            data['cmd'] = 'approved'
        elif enabled is False:
            data['cmd'] = 'disapproved'
        else:
            raise ValueError
        r = self.session.post(posturl, json=data)
        if r.status_code == 200:
            j = r.json()
            if j['meta']['rc'] == 'ok':
                return True
            return False
        # status_code 400 for bad password
        return False

    def upgrade_unifi(self, site='default'):
        """ send upgrade-controller-package command
            to trigger an upgrade of unifi controller
            this takes a long time to return from the POST

        """
        posturl = self._build_url('system', site)
        data = {'cmd': 'update-controller-package'}
        if posturl is False:
            raise ValueError
        r = self.session.post(posturl, json=data)
        if r.status_code == 200:
            j = r.json()
            if j['meta']['rc'] == 'ok':
                return True
            return False
        # status_code 400 for bad password
        return False

    def get_unifi_upgrade_progress(self, site='default'):
        """ get upgrade progress
        """
        import json
        import requests
        geturl = self._build_url('upgrade-progress', site)
        try:
            r = self.session.get(geturl)
        except requests.exceptions.RequestException:
            return False
        if r.status_code == 401:
            self.renew_login()
            if self._login() is False:
                return False
            try:
                r = self.session.get(geturl)
            except requests.exceptions.RequestException:
                return False
        if r.status_code == 200:
            try:
                content = r.json()
            except json.JSONDecodeError:
                return False
            if 'data' in content:
                return content['data'][0]['cloudkey_unifi_updating_progress']
        return False

    def get_unifi_info(self, site='default'):
        """ get unifi information
        """
        geturl = self._build_url('sysinfo', site)
        r = self.session.get(geturl)
        if r.status_code == 200:
            j = r.json()
            if 'data' in j:
                return j['data'][0]
        return False

    # misc
    def get_state(self, code):
        """ get_state returns text value of state code
        """
        if code in self.DEVICE_STATE:
            return self.DEVICE_STATE[code]
        return False

    def wait_status(self, site, mac, target_state):
        """ wait indefinitely for the specified
            device to reach the desired status
        """
        import time
        tik = 0
        print('Waiting for device {} to become {}'.format(mac, self.DEVICE_STATE[target_state]), end='')
        device_status = self.get_device_by_mac(site, mac)
        while device_status is False or not device_status or device_status[0]['state'] != target_state:
            print('.', end='', flush=True)
            tik += 1
            if not tik % 25:
                if device_status is not False and device_status and device_status[0]['state']:
                    print('[State={}]'.format(device_status[0]['state']), end='')
            time.sleep(1)
            device_status = self.get_device_by_mac(site, mac)
        print('done.')
        return True

    def get_previous_firmware_info(self, model, versionsback=1, channel='release'):
        """ use get_previous_firmware to check what is the previous firmware version
            -1 versionsback returns the oldest available version
            for this model
            channels: release, beta-public
                      (dev only has 1 product, unifi-protect-box for android)
        """
        from datetime import datetime
        if model is False:
            return False
        if channel not in ['release', 'beta-public', 'dev']:
            return False
        url = 'https://fw-update.ubnt.com/api/firmware'
        url += '?filter=eq~~channel~~' + channel
        url += '&filter=eq~~platform~~' + model
        data = self._read_api(url, None, False)
        firmware_list = data['_embedded']['firmware']
        if versionsback >= len(firmware_list):
            return False
        sorted_list = sorted(firmware_list, reverse=True,
                             key=lambda x: datetime.strptime(x['updated'][:19], '%Y-%m-%dT%H:%M:%S'))
        if versionsback == -1:
            f = sorted_list.pop(0)
            return f['version'], f['_links']['data']['href']
        f = sorted_list.pop(0)
        for _ in range(versionsback):
            f = sorted_list.pop(0)
        return f['version'], f['_links']['data']['href']

if __name__ == '__main__':
    # connect to your test system with a wired connection
    # so you are not kicked off when the disruptive Unifi
    # tests are run
    #
    # test environment requirements
    # Unifi controller
    # 1 USG configured
    # at least 1 UAP configured, the first AP needs to be adopted
    # at least 1 WLAN configured
    # room to add at least 1 more WLAN
    # 1 site configured, or the UAP in the first site found
    # at least 1 client connected to each WLAN that is active
    # SSIDs 'test' and 'testwlanname' not already in use
    # there is one alarm in the alarm history (restart the USG to create one)
    #
    # not tested: switch settings
    #
    import getpass
    test_unifi_server = input('Unifi controller hostname: [localhost] ') or 'localhost'
    test_unifi_port = input('Port: [8443] ') or 8443
    assert(str(test_unifi_port) == str(int(test_unifi_port))), 'Invalid port'
    assert(int(test_unifi_port) in range(1, 66536)), 'Invalid port'
    test_unifi_user = input('User name: [admin] ') or 'admin'
    try:
        test_unifi_password = getpass.getpass()
    except Exception as error:
        print('ERROR', error)
        raise RuntimeError
    # first test explicit verify False
    api = pyUnifiAPI(test_unifi_user,
                     test_unifi_password,
                     test_unifi_server,
                     test_unifi_port,
                     False)
    assert(api.get_logged_in_user() is not False), 'Failed login test 1'
    assert(api.logout()), 'Failed logout test 1'
    assert(api.test_build_url('status') is not False), 'Failed _build_url test 1 (valid)'
    assert(api.test_build_url('fail') is False), 'Failed _build_url test 2 (invalid)'
    assert(api.test_build_url('status', 'fail') is False), 'Failed _build_url test 3 (invalid)'
    assert(api.test_build_url('evtmgt', None) is False), 'Failed _build_url test 4'
    assert(api.test_build_url('evtmgt', 'test') is not False), 'Failed _build_url test 5'
    assert(api.test_get_wlan_id('test', 'test') is False), 'Failed test _get_wlan_id 1'

    # next test with default verify value
    api = pyUnifiAPI(test_unifi_user,
                     test_unifi_password,
                     test_unifi_server,
                     test_unifi_port)
    test_username = api.get_logged_in_user()
    assert(test_username is not False), 'Failed login test'
    print('Successfully logged in as {}'.format(test_username))
    # test get_unifi_info
    assert(api.get_unifi_info() is not False), 'Failed test get_unifi_info'
    # sites tests
    test_unifi_sites = api.get_sites()
    print('Sites: {}'.format(', '.join(test_unifi_sites)))
    assert(test_unifi_sites), 'No sites returned'
    test_first_site = test_unifi_sites[0]
    test_first_site_details = api.get_site_by_id(test_first_site)
    assert(test_first_site_details is not False), 'Failed to get site details'
    test_second_site_desc = 'testsecondsite'
    test_second_site_desc_renamed = 'testsecondsiterenamed'
    assert(api.add_site(test_second_site_desc) is not False), 'Failed to add a site'
    assert(api.rename_site(api.get_site_by_description(test_second_site_desc), test_second_site_desc_renamed) is not False), 'Failed to rename a site'
    assert(api.remove_site(api.get_site_by_description(test_second_site_desc_renamed)) is not False), 'Failed to remove test site'

    # logout test
    assert(api.logout()), 'Failed logout test'
    username = api.get_logged_in_user()
    assert(api.get_logged_in_user() is False), 'Failed logout confirm test'
    # renew login test
    assert(api.renew_login()), 'Failed renew_login test'
    # more sites tests
    test_unifi_site = test_unifi_sites[0].strip() # first one found
    print("testing with site {}".format(test_unifi_site))
    # device tests
    test_unifi_devices = api.get_devices_quick(test_unifi_site)
    assert(test_unifi_devices), 'No devices returned in site (quick)'
    test_unifi_devices = api.get_devices(test_unifi_site)
    assert(test_unifi_devices), 'No devices returned in site (all)'
    test_unifi_device = test_unifi_devices[0]['name']
    test_unifi_mac = test_unifi_devices[0]['mac'] # the first device found
    print("test device: {}".format(test_unifi_device))
    test_unifi_device_detail = api.get_device_by_mac(test_unifi_site,
                                                     test_unifi_mac)
    assert(len(test_unifi_device_detail) == 1), 'Failed to return 1 device in result by MAC address'
    assert(test_unifi_device_detail), 'Failed to get device detail by MAC address'
    assert(test_unifi_device_detail[0]['name'] == test_unifi_device), 'Got wrong device in results by MAC address'
    test_unifi_device_detail = api.get_device_by_name(test_unifi_site,
                                                      test_unifi_device)
    assert(len(test_unifi_device_detail) == 1), 'Failed to return 1 device in results by name'
    assert(test_unifi_device_detail), 'Failed to get device detail by name'
    assert(test_unifi_device_detail[0]['name'] == test_unifi_device), 'Got wrong device in results by name'
    test_unifi_device_overview = api.get_device_overview_by_name(test_unifi_site, test_unifi_device)
    assert(test_unifi_device_overview), 'Failed to get device overview by name'
    assert(test_unifi_device_overview['name'] == test_unifi_device), 'Got wrong device name in results by site overview'
    for loop in test_unifi_devices: # test all types
        if 'name' in loop:
            udo = api.get_device_overview_by_name(test_unifi_site, loop['name'])
            assert(udo), "Failed device overview: {}".format(loop['name'])
        else:
            print("Skipping unnamed device with MAC {}".format(loop['mac']))
    for t in ['uap', 'ugw']:
        test_unifi_dev_list = api.get_devices(test_unifi_site, t)
        assert(test_unifi_dev_list), "Failed to get devices by type {}".format(t)
    test_unifi_usgs = api.get_devices(test_unifi_site, 'ugw')
    assert(len(test_unifi_usgs) == 1), 'Failed to find USGs'
    test_unifi_usg = test_unifi_usgs[0]
    test_unifi_usg_name = test_unifi_usg['name']
    assert(test_unifi_usg_name), 'Failed to find USG name'
    test_usg_ports = api.get_ports(test_unifi_site, test_unifi_usg_name)
    assert(test_usg_ports is not False), 'Failed to get USG ports'
    test_unifi_aps = api.get_devices_brief_by_type(test_unifi_site, 'uap')
    assert(len(test_unifi_aps) > 0), 'Failed to find APs'
    test_unifi_aps = api.get_devices(test_unifi_site, 'uap')
    for unifi_ap_uplink_test in test_unifi_aps:
        test_unifi_uplinks = api.get_uplink(test_unifi_site, unifi_ap_uplink_test['name'])
        assert(test_unifi_uplinks), 'Failed to get uplinks'
    if 'name' in test_unifi_aps[0]:
        test_unifi_connected_to_ap = api.get_clients_by_ap(test_unifi_site,
                                                           test_unifi_aps[0]['name'])
    else:
        test_unifi_connected_to_ap = api.get_clients_by_ap_mac(test_unifi_site, test_unifi_aps[0]['mac'])
    assert(test_unifi_connected_to_ap), 'Failed to find devices connected to the first AP'
    if 'name' in test_unifi_aps[0]:
        print("Client MACs connected to AP {}".format(test_unifi_aps[0]['name']))
    else:
        print("Client MACs aconnect to unnamed API with MAC {}".format(test_unifi_aps[0]['mac']))
    for a_client in test_unifi_connected_to_ap:
        print(" [+] {}".format(a_client))
    test_unifi_wlan_groups = api.get_wlan_groups(test_unifi_site)
    print('WLAN groups:')
    for wg in test_unifi_wlan_groups:
        print(" [+] {}".format(wg['name']))
    assert(test_unifi_wlan_groups), 'Failed to get wlan groups'
    test_unifi_wlan_group = test_unifi_wlan_groups[0]['name']
    test_unifi_wlan_group_id = test_unifi_wlan_groups[0]['_id']
    assert(test_unifi_wlan_group_id), 'Failed to set a wlan group'
    assert(api.test_get_wlan_id(test_unifi_site, 'test') is False), 'Failed test _get_wlan_id 2'
    test_unifi_wlans = api.get_wlans(test_unifi_site)
    assert(test_unifi_wlans), 'Failed to get wireless lans'
    for wl in test_unifi_wlans:
        print(wl['name'])
        if 'enabled' in wl:
            print(" [+] Enabled: {}".format(wl['enabled']))
        else:
            print(" [-] Enabled: False")
        print(" [+] WLAN ID: {}".format(wl['_id']))
        if 'is_guest' in wl and wl['is_guest'] is True:
            print(" [+] Guest network: true")
        else:
            print(" [+] Guest network: false")
        if 'security' in wl:
            print(" [+] Security: {}".format(wl['security']))
            if wl['security'] == 'wpapsk':
                print(" [+] WPA mode: {} / {}".format(wl['wpa_mode'], wl['wpa_enc']))
    test_unifi_wlan = test_unifi_wlans[0]['name']
    test_unifi_details = api.get_wlan_details(test_unifi_site, test_unifi_wlan)
    assert(test_unifi_details), 'Failed to get WLAN details by name'
    test_unifi_wlan_id = test_unifi_details['_id']
    assert(api.get_wlan_details_by_id(test_unifi_site, test_unifi_wlan_id)), 'Failed to get wlan details by id'
    test_unifi_clients = api.get_clients(test_unifi_site, False)
    assert(test_unifi_clients), 'Failed to get all clients'
    test_unifi_clients = api.get_clients(test_unifi_site)
    assert(test_unifi_clients), 'Failed to get active clients'
    print('Client hostnames:')
    for u in test_unifi_clients:
        if 'hostname' in u:
            h = u['hostname']
        else:
            h = 'N.A.'
        print(" [+] {} [{}]".format(h, u['mac']))
    test_unifi_client_mac = test_unifi_clients[0]['mac']
    test_unifi_client_name = test_unifi_clients[0]['name']
    test_unifi_client = api.get_client(test_unifi_site, test_unifi_client_mac)
    assert(test_unifi_client), 'Failed client details test'
    assert(test_unifi_client_name == test_unifi_client[0]['hostname']), 'Failed correct client check'
    test_unifi_user_groups = api.get_user_groups(test_unifi_site)
    assert(test_unifi_user_groups), 'Failed to get user groups'
    test_unifi_user_group = test_unifi_user_groups[0]['name']
    assert(test_unifi_user_group), 'Failed to select a user group'
    print("Selected user group: {}".format(test_unifi_user_group))
    test_unifi_user_group_id = test_unifi_user_groups[0]['_id']
    assert(test_unifi_user_group_id), 'Failed to select a user group id'
    test_alarms = api.get_alarms(test_unifi_site)
    assert(test_alarms), 'No recent alarms'
    test_alarms = api.get_alarms(test_unifi_site, True) # all alarms, not recent
    assert(test_alarms), 'No alarms (ever)'
    assert(api.renew_login()), 'Failed renew_login test'
    test_ap_mac = test_unifi_aps[0]['mac']
    if 'name' in test_unifi_aps[0]:
        test_ap_name = test_unifi_aps[0]['name']
    else:
        test_ap_name = "{}-unnamed".format(test_unifi_aps[0]['mac'])
    test_ap_id = test_unifi_aps[0]['device_id']
    print(('About to test WLAN functionality. Access points and the gateway '
           'will provision, affecting WLAN connectivity.'))
    test_unifi_testwlanname = 'testwlanname'
    test_unifi_testwlanpassword = 'testwlanpassword'
    test_unifi_testwlanpassword2 = 'testwlanpasswordi2'
    test_ap_groups = api.get_ap_groups(test_unifi_site)
    test_ap_group_id = test_ap_groups[0]['_id']
    assert(api.create_wlan_psk(test_unifi_site,
                               test_unifi_testwlanname,
                               test_unifi_testwlanpassword,
                               test_unifi_wlan_group_id,
                               test_unifi_user_group_id,
                               test_ap_group_id)), 'Failed to create WLAN'
    assert(api.disable_wlan(test_unifi_site, test_unifi_testwlanname)), 'Failed to disable WLAN'
    assert(api.enable_wlan(test_unifi_site, test_unifi_testwlanname)), 'Failed to enable WLAN (success)'
    assert(api.enable_wlan(test_unifi_site, 'fail') is False), 'Failed to test enable WLAN (failure)'
    assert(api.set_wlan_password(test_unifi_site, test_unifi_testwlanname, test_unifi_testwlanpassword2) is True), 'Failed to change password'
    test_wlan_check_details = api.get_wlan_details(test_unifi_site, test_unifi_testwlanname)
    assert(test_wlan_check_details['x_passphrase'] == test_unifi_testwlanpassword2), 'Failed to verify password was changed'
    assert(api.remove_wlan(test_unifi_site, test_unifi_testwlanname)), 'Failed to remove WLAN'
    print("About to restart device {} ({})".format(test_ap_name, test_ap_mac))
    assert(api.restart_device(test_unifi_site, test_ap_mac)), 'Failed to restart AP'
    status = api.get_device_by_mac(test_unifi_site, test_ap_mac)
    assert(api.wait_status(test_unifi_site, test_ap_mac, 5)), 'Failed to see AP provisioning'
    assert(api.wait_status(test_unifi_site, test_ap_mac, 1)), 'Failed to come up after restart'
    print('About to remove/forget device {} ({})'.format(test_ap_name, test_ap_mac))
    assert(api.remove_device_from_site(test_unifi_site, test_ap_mac)), 'Failed to forget AP'
    assert(api.wait_status(test_unifi_site, test_ap_mac, 2)), 'Failed to find AP ready for adoption'
    print('About to adopt device {} ({})'.format(test_ap_name, test_ap_mac))
    assert(api.adopt_device(test_unifi_site, test_ap_mac)), 'Failed to adopt AP'
    assert(api.wait_status(test_unifi_site, test_ap_mac, 5)), 'Failed to see AP provisioning'
    assert(api.wait_status(test_unifi_site, test_ap_mac, 1)), 'Failed to see AP come ready'
    print('About to rename device back to original name {} ({})'.format(test_ap_name, test_ap_mac))
    test_detail = api.get_device_by_mac(test_unifi_site, test_ap_mac)
    test_ap_id = test_detail[0]['device_id']
    assert(api.rename_device(test_unifi_site, test_ap_id, test_ap_name)), 'Failed to rename device'
    assert(api.wait_status(test_unifi_site, test_ap_mac, 5)), 'Failed to see AP provisioning'
    assert(api.wait_status(test_unifi_site, test_ap_mac, 1)), 'Failed to see AP come ready'
    test_details = api.get_device_by_mac(test_unifi_site, test_ap_mac)
    assert(test_details[0]['name'] == test_ap_name), 'Failed test to confirm rename successful'
    assert(api.set_analytics('default', False) is True), 'Failed test to turn off analytics'
    # user group tests
    assert(api.add_user_group(test_unifi_site, 'test_user_group', 10000, 1000) is True), 'Failed test add_user_group'
    assert(api.modify_user_group(test_unifi_site, 'test_user_group', 'test_user_group', -1, -1) is True), 'Failed test modify_user_group'
    assert(api.delete_user_group(test_unifi_site, 'test_user_group') is True), 'Failed test to delete_user_group'

    # add more tests here
    print('Passed tests')