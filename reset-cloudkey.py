#!/usr/bin/env python3
""" reset Ubiquitii Unifi CloudKey devices
    to factory defaults
"""
import sys
import getpass
from pycloudkeyapi import pyCloudkeyApi
worklist = []
discovered = []
api = pyCloudkeyApi()
for a in sys.argv:
    if a == 'all':
        print('Discovering CloudKeys...')
        allkeys = api.discover()
        for k in allkeys:
            print('discovered: {}'.format(k['ip']))
            if k['ip'] not in discovered:
                discovered.append(k['ip'])
    else:
        if a not in worklist:
            worklist.append(a)
worklist.pop(0)
for d in discovered:
    if d not in worklist:
        worklist.append(d)
if not worklist:
    worklist.append(input('IP of CloudKey to reset to factory defaults: '))
for w in worklist:
    print('Resetting CloudKey to factory defaults: ' + w)
    creds = api.get_creds(w)
    if creds is False:
        print('enter credentials')
        username = input('Username: ')
        password = getpass.getpass()
    else:
        username, password = creds
    result = api.reset_cloudkey(w, username, password)
    assert(result is True), 'reset failed, are the credentials correct?'
    assert(api.isup(w) is True), ' failed to come up after reset'
    print('reset complete for {}'.format(w))
print('Program complete.')