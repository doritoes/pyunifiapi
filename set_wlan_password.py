#!/usr/bin/env python3
""" set WLAN password

    If there is only one site, that site is used
    If there is more than one site and the WLAN
    SSID only exists in one site, that site is used.
    Specifying the site is optional unless required
    to solve ambiguity between sites.
"""
if __name__ == '__main__':
    import sys
    import getpass
    import argparse
    from pyunifiapi import pyUnifiAPI

    # parse arguments and request missing data from user
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--server", required=False,
                        action="store",
                        help="Unifi controller name or IP address")
    parser.add_argument("--port", required=False,
                        action="store",
                        help="Unifi port (default 8443)")
    parser.add_argument("-u", "--username",
                        action="store",
                        required=False,
                        help="Username")
    parser.add_argument("-p", "--password",
                        action="store",
                        required=False,
                        help="Password")
    parser.add_argument("--ssid",
                        action="store",
                        required=False,
                        help="SSID to change password for")
    parser.add_argument("--passphrase",
                        action="store",
                        required=False,
                        help="passphrase to set WLAN to")
    parser.add_argument("--site",
                        action="store",
                        required=False,
                        help="site name")
    parser.add_argument("-t", "--test",
                        action="store_true",
                        help=("Perform a dry run without "
                              "making any changes."))

    args = parser.parse_args()
    if args.port:
        unifi_port = args.port
    else:
        unifi_port = 8443
    if args.server:
        unifi_server = args.server
    else:
        unifi_server = input('Unifi controller hostname [localhost]: ') or 'localhost'
    if args.username:
        unifi_username = args.username
    else:
        unifi_username = input('User name [admin]: ') or 'admin'
    if args.password:
        unifi_password = args.password
    else:
        try:
            unifi_password = getpass.getpass()
        except Exception as error:
            print('ERROR', error)
            raise RuntimeError
    if args.ssid:
        unifi_ssid = args.ssid
    else:
        unifi_ssid = input('SSID name [secure]: ') or 'secure'
    if args.passphrase:
        unifi_passphrase = args.passphrase
    else:
        unifi_passphrase = input('Passphrase for SSID [secure]: ') or 'secure'
    if args.site:
        unifi_site = args.site
    else:
       unifi_site = False

    # instantiate api
    api = pyUnifiAPI(unifi_username,
                     unifi_password,
                     unifi_server,
                     unifi_port)
    if not api.valid_passphrase(unifi_passphrase):
        print("Invalid passphrase specified: {}".format(unifi_passphrase))
        print("Minimum 8 characters, maximum 63 characters")
        sys.exit()
    sites = api.get_sites()
    wlan_site_matches = []
    selected_site = False
    for site in sites:
        wlans = api.get_wlans(site)
        wlan_list = []
        for wlan in wlans:
            wlan_list.append(wlan['name'])
        if unifi_ssid in wlan_list:
            wlan_site_matches.append(site)
    if not wlan_site_matches:
        if len(sites) > 1:
            print("SSID {} not found in any of the sites:".format(unifi_ssid))
            print("\n".join(sites))
        else:
            if sites:
                print("SSID {} not found in site {}".format(unifi_ssid, sites[0]))
            else:
                print("ERROR: no sites found")
        sys.exit()
    if len(wlan_site_matches) > 1:
        if unifi_site is False:
            print(("SSID {} matched multiple sites. Specify "
                   "which using --site"))
            print("\n".join(wlan_site_matches))
            sys.exit()
        else:
            if unifi_site not in wlan_site_matches:
                print(("SSID {} was not found in "
                       "site {}").format(unifi_ssid, unifi_site))
                print("\n".join(wlan_site_matches))
                sys.exit()
    else:
        if unifi_site is not False:
            if unifi_site not in wlan_site_matches:
                print(("SSID {} was not found in "
                       "site {}").format(unifi_ssid, unifi_site))
                print("\n".join(wlan_site_matches))
                sys.exit()
            unifi_site = wlan_site_matches[0]
        else:
            unifi_site = wlan_site_matches[0]

    wlan_details = api.get_wlan_details(unifi_site, unifi_ssid)
    old_passphrase = wlan_details['x_passphrase']
    if unifi_passphrase == old_passphrase:
        print('Attempting to set the same password. Aborting.')
        sys.exit()

    # making the changes
    if args.test:
        print('Testing mode (dry run) selected')
        print('Would have changed the password:')
        print("Site: {}".format(unifi_site))
        print("SSID: {}".format(unifi_ssid))
        print("Password: {}".format(unifi_passphrase))
    else:
        print("Making change...")
        print("Site: {}".format(unifi_site))
        print("SSID: {}".format(unifi_ssid))
        print("Password: {}".format(unifi_passphrase))
        if api.set_wlan_password(unifi_site,
                                 unifi_ssid,
                                 unifi_passphrase) is not True:
            print('Failed to set new password')
            sys.exit()
        # confirm the change
        wlan_check_details = api.get_wlan_details(unifi_site,
                                                  unifi_ssid)
        if wlan_check_details['x_passphrase'] != unifi_passphrase:
            print(('ERROR: The changed password does not match '
                   'the expected value.'))
            sys.exit()
        print('Password changed successfully.')