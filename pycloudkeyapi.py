#!/usr/bin/env python3
""" prototype python class to manage basic cloudkey operations

    requires module paramiko
    #python3 -m pip install paramiko
"""
import time
import paramiko

class AllowAllKeys(paramiko.MissingHostKeyPolicy):
    """ allows ssh to devices with unrecognized keys,
        something we need to do with new default
        cloudkeys
    """
    def missing_host_key(self, client, hostname, key):
        """ allow missing host key
        """
        return

class pyCloudkeyApi:
    """ provide access to UniFi cloudkey setup
    """
    __version__ = '0.5'
    __documentation__ = 'https://gitlab.com/doritoes/pyunifiapi'
    # store common default and test passwords to help reset CloudKeys
    CREDS = [('ubnt', 'ubnt'),
             ('admin', 'password'),
             ('localadmin', 'password'),
             ('ubnt', 'password'),
             ('root', 'ubnt'),
             ('admin', 'ubnt'),
             ('root', 'root'),
             ('admin', 'admin')]

    # functions
    @staticmethod
    def _read_api(path, params=None, verify=None):
        """ use _read_api to read a JSON resources using
            simple GET
        """
        import json
        import requests
        s = requests.session()
        s.headers = {'content-type': 'application/json'}
        requests.packages.urllib3.disable_warnings()
        if verify is False:
            s.verify = False
        elif verify is None:
            pass
        else:
            s.verify = verify
        if params is None:
            r = s.get(path)
        else:
            try:
                r = s.get(path, params=params)
            except requests.exceptions.RequestException:
                return False
        if r.status_code != 200:
            return False
        try:
            j = r.json()
        except json.JSONDecodeError:
            return False
        return j

    @staticmethod
    def _post_api(path, data, verify=None):
        """ use _post_api to POST to an API path
        """
        import requests
        s = requests.session()
        s.headers = {'content-type': 'application/json'}
        requests.packages.urllib3.disable_warnings()
        if verify is False:
            s.verify = False
        elif verify is None:
            pass
        else:
            s.verify = verify
        r = s.post(path, json=data)
        if r.status_code != 200:
            return False
        return True

    @staticmethod
    def _test_url(path):
        """ use _test_url to test if able to load
            the path (status 200 is successful)
        """
        import requests
        try:
            r = requests.get(path)
        except (requests.exceptions.InvalidSchema,
                requests.exceptions.ConnectionError):
            return False
        return r.status_code == 200

    @staticmethod
    def _test_ssh_simple(host, user, password, port=22):
        """ use _test_ssh_simple to do a simple ssh
            authentication test
        """
        try:
            t = paramiko.Transport((host, port))
            t.connect(username=user, password=password)
        except paramiko.AuthenticationException:
            #print("DEBUG: failed authentication") # debug
            return False
        except paramiko.SSHException:
            #print("DEBUG: failed SSH connection") # debug
            return False
        return True

    @staticmethod
    def get_results_ssh_command(host, user, password, command, port=22, timeout=None):
        """ use get_results_ssh_command to run a command
            on a remote system using paramiko and return
            the result
        """
        import os
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        try:
            client.load_host_keys(os.path.expanduser('~/.ssh/known_hosts'))
        except FileNotFoundError:
            pass
        client.set_missing_host_key_policy(AllowAllKeys())
        try:
            client.connect(host, username=user, password=password, port=port)
        except paramiko.AuthenticationException:
            print("failed authentication")
            return False
        except paramiko.SSHException:
            print("failed ssh connection")
            return False
        except:
            print("other error, like NoValidConnectionsError")
            return False
        if timeout is None:
            dummy_stdin, stdout, dummy_stderr = client.exec_command(command, get_pty=True)
        else:
            try:
                dummy_stdin, stdout, dummy_stderr = client.exec_command(command, get_pty=True, timeout=timeout)
            except:
                return False
        output = []
        for line in iter(stdout.readline, ""):
            output.append(line)
        return output

    # internal methods
    def __init__(self, mx=3):
        """ __init__ is the init method """
        self.ssdp_mx = mx
        self.cloudkeys = []

    def __str__(self):
        """ __str__ is the str method """
        return self.__repr__()

    def __repr__(self):
        """ __repr__ is the repr method """
        return '{} object'.format(self.__class__.__name__)

    # external methods
    def isup(self, host, user='ubnt', password='ubnt',
             port=22, initial=60, cycle=15, maxtime=720):
        """ use isup to wait for a CloudKey to become ready
            ("up"). first test if a ssh connection succeeds,
            then test if the controller web interface is up
            test ssh to a CloudKey and if that is
            successful, theck up check for the Unifi
            controller stats page to be up
        """
        time.sleep(initial)
        elapsed = initial
        while self.isup_ssh(host, user, password, port) is False or self.isup_controller(host) is False:
            if elapsed >= maxtime:
                return False
            elapsed += cycle
            time.sleep(cycle)
        return True

    def isup_ssh(self, host, user='ubnt', password='ubnt', port=22):
        """ use isup_ssh to do a simple ssh connection test
        """
        return self._test_ssh_simple(host, user, password, port)

    def isup_controller(self, host, port=8443):
        """ use isup_controller to test if the unifi controller
            is responding. first a socket test, then a request
        """
        import socket
        a_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        if a_socket.connect_ex((host, port)) == 0:
            path = "https://{}:{}/status".format(host, port)
            return self._read_api(path, None, False)
        return False

    def get_creds(self, host, port=22):
        """ use get_creds to try a number of default and common
            credentials
        """
        for credential in self.CREDS:
            username, password = credential
            if self.isup_ssh(host, username, password, port):
                return credential
        return False

    def check_new_device_upgrade(self, host, username='ubnt', password='ubnt'):
        """ use check_new_device_upgrade to check if
            devices appears to be a brand new device
            that needs updated firmware
        """
        from distutils.version import StrictVersion
        if not self.test_cloudkey_default_ssh_credentials(host, username, password):
            return False
        product, version, dummy_firmware = self.test_cloudkey_firmware_default(host,
                                                                               username,
                                                                               password)
        latest_version, dl_link = self.get_latest_firmware_info(product)
        if StrictVersion(latest_version) > StrictVersion(version):
            return dl_link
        return False

    def get_controller_info(self, host):
        """ use get_controller_info to get information from
            a unifi controller. notable contents of data[]
            (not all listed)
              cloudkey_version: string
              cloudkey_update_available: boolean
              package_version: string
              package_update_available: boolean
              package_update_version: string
              ip_addrs: list of IPs
              name: "Unifi"
              cloudkey_sdcard_mounted: boolean
              timezone: "UTC"
              version: string (controller)
              ubnt_device_type: UCKG2
              hostname: unifi.yourdomain.com
              update_available: boolean
        """
        url = "https://{}:8443/api/stat/sysinfo".format(host)
        data = self._read_api(url, None, False)
        return False if data is False else data

    def get_latest_firmware_info(self, model, channel='release'):
        """ use get_latest_firmware_info to check what is the latest firmware
            version for this model
        """
        import re
        if model is False:
            return False
        if channel not in ['release', 'beta-public', 'dev']:
            return False
        url = 'https://fw-update.ubnt.com/api/firmware-latest'
        url += '?filter=eq~~channel~~' + channel
        url += '&filter=eq~~platform~~' + model
        data = self._read_api(url, None, False)
        if data is False:
            return False
        try:
            firmware_version = data['_embedded']['firmware'][0]['tags']['ubnt_version']
            firmware_dl_link = data['_embedded']['firmware'][0]['_links']['data']['href']
        except:
            return False
        r = re.compile(r'^[A-Z0-9]+\.[a-z0-9]+\.v([0-9]+\.[0-9]+\.[0-9]+)\.')
        m = r.search(firmware_version)
        if m:
            return m.group(1), firmware_dl_link
        return False

    def get_previous_firmware_info(self, model, versionsback=1, channel='release'):
        """ use get_previous_firmware to check what is the previous firmware version
            for this model
            channels: release, beta-public
                      (dev only has 1 product, unifi-protect-box for android)
        """
        import re
        from datetime import datetime
        if model is False:
            return False
        if channel not in ['release', 'beta-public', 'dev']:
            return False
        url = 'https://fw-update.ubnt.com/api/firmware'
        url += '?filter=eq~~channel~~' + channel
        url += '&filter=eq~~platform~~' + model
        data = self._read_api(url, None, False)
        firmware_list = data['_embedded']['firmware']
        if versionsback >= len(firmware_list):
            return False
        sorted_list = sorted(firmware_list, reverse=True,
                             key=lambda x: datetime.strptime(x['updated'][:19], '%Y-%m-%dT%H:%M:%S'))
        if versionsback == -1:
            f = sorted_list.pop()
        else:
            f = sorted_list.pop(0)
            for _ in range(versionsback):
                f = sorted_list.pop(0)
        if 'tags' in f and 'ubnt_version' in f['tags']:
            firmware_version = f['tags']['ubnt_version']
            r = re.compile(r'^[A-Z0-9]+\.[a-z0-9]+\.v([0-9]+\.[0-9]+\.[0-9]+)\.')
            m = r.search(firmware_version)
            if m:
                return m.group(1), f['_links']['data']['href']
            return False
        firmware_version = f['version']
        r = re.compile(r'^v([0-9\.]+)\+\d+$')
        m = r.search(firmware_version.strip())
        if m:
            return m.group(1), f['_links']['data']['href']
        return False

    def test_ssdp_location(self, path):
        """ use test_ssdp_location to test if able to
            load the SSDP 'Location'
        """
        return self._test_url(path)

    def test_cloudkey_default_ssh_credentials(self, host, username='ubnt', password='ubnt'):
        """ use test_cloudkey_default_credentials to
            test if the discovered cloudkey is using the
            default credentials of ubnt/ubnt
        """
        return self._test_ssh_simple(host, username, password)

    def test_cloudkey_firmware_default(self, host, username='ubnt', password='ubnt'):
        """ use test_cloudkey_firmware_default to
            return the firmware version (on a default system)
        """
        import re
        output = self.get_results_ssh_command(host,
                                              username,
                                              password,
                                              'cat /usr/lib/version')
        if output is False:
            return False, False, False
        firmware = output[0]
        r = re.compile(r'^([A-Z0-9]+)\.[a-z0-9]+\.v([0-9]+\.[0-9]+\.[0-9]+)\.')
        m = r.search(firmware)
        if m:
            return m.group(1), m.group(2), firmware
        return False, False, False

    def reboot_cloudkey(self, host, user='ubnt', password='ubnt'):
        """ use reboot_cloudkey to reboot the cloudkey
            using the same command in ubnt-systool
        """
        command = '/sbin/reboot || /sbin/reboot -f'
        result = self.get_results_ssh_command(host, user, password, command)
        return not result is False

    def poweroff_cloudkey(self, host, user='ubnt', password='ubnt'):
        """ use poweroff_cloudkey to power off the cloudkey
            using the same command in ubnt-systool
        """
        command = '/sbin/poweroff || /sbin/poweroff -f'
        result = self.get_results_ssh_command(host, user, password, command)
        return not result is False

    def reset_cloudkey(self, host, user='ubnt', password='ubnt'):
        """ use reset_cloudkey to reset cloudkey to factory
            default using ssh command
        """
        command = '/sbin/ubnt-systool reset2defaults'
        result = self.get_results_ssh_command(host, user, password, command)
        return not result is False

    def set_cloudkey_name(self, host, hostname, user='ubnt', password='ubnt'):
        """ use set_cloudkey_name to set the cloudkey device name
        """
        command = '/sbin/ubnt-systool hostname ' + hostname
        result = self.get_results_ssh_command(host, user, password, command)
        return not result is False

    def set_cloudkey_timezone(self, host, timezone, user='ubnt', password='ubnt'):
        """ use set_cloudkey_name to set the cloudkey timezone
            valid timezones are found in /usr/share/zoneinfo
        """
        command = '/sbin/ubnt-systool timezone ' + timezone
        result = self.get_results_ssh_command(host, user, password, command)
        return not result is False

    def set_cloudkey_admin_name(self, host, admin, username='ubnt', password='ubnt'):
        """ use set_cloudkey_admin_name to set the cloudkey admin
            user username

            this is overwritten by set_controller_admin
        """
        command = '/sbin/ubnt-systool adminname ' + admin
        result = self.get_results_ssh_command(host, username, password, command)
        return not result is False

    def set_cloudkey_admin_email(self, host, email,
                                 username='ubnt', password='ubnt'):
        """ use set_cloudkey_admin_email to set the cloudkey admin
            user email

            this is overwritten by set_controller_admin
        """
        command = '/sbin/ubnt-systool adminemail ' + email
        result = self.get_results_ssh_command(host, username, password, command)
        return not result is False

    def set_cloudkey_admin_password(self, host, adminpass,
                                    username='ubnt', password='ubnt'):
        """ use set_cloudkey_admin_email to set the cloudkey admin
            user email

            this is overwritten by set_controller_admin
        """
        command = "echo -e '" + username + ":" + adminpass + "'"
        command += " | /sbin/ubnt-systool chpasswd"
        result = self.get_results_ssh_command(host, username, password, command)
        return not result is False

    def set_cloudkey_led(self, host, username='ubnt', password='ubnt', color='blue',
                         action='on', brightness=255, timeron=50, timeroff=255):
        """ use set_cloudkey_led to use ubnt-systool to trigger LEDs
        """
        outcome = True
        if color not in ('white', 'blue', 'all'):
            return False
        colors = []
        if color == 'all':
            colors.extend(['white', 'blue'])
        else:
            colors.append(color)
        try:
            b = int(brightness)
        except ValueError:
            return False
        if b not in range(256) or int(brightness) != brightness:
            return False
        for c in colors:
            if action not in ('on', 'off', 'settimer'):
                return False
            command = '/sbin/ubnt-systool led ' + c
            if action in ('on', 'off'):
                command += ' ' + action
                if command == 'on':
                    command += ' ' + brightness
                result = self.get_results_ssh_command(host, username, password, command)
                if result is False:
                    outcome = False
            else:
                command += ' {} {} {}'.format(action, timeron, timeroff)
                result = self.get_results_ssh_command(host, username, password, command)
                if result is False:
                    outcome = False
        return outcome

    def set_cloudkey_reset_button(self, host, enabled, username='ubnt', password='ubnt'):
        """ use set_cloudkey_reset_button to enable/disable the
            physical reset button on a CloudKey
        """
        command = '/sbin/ubnt-systool resetbutton '
        if enabled is False:
            command += 'false'
        else:
            command += 'true'
        result = self.get_results_ssh_command(host, username, password, command)
        if result is False:
            return False
        # to be truly sure, check contents of /etc/default/infctld
        # INFCTLD_ARGS=-d means reset button enabled
        # INFCTLD_ARGS=-d -n means reset button disabled
        return True

    def get_cloudkey_mac(self, host, username='ubnt', password='ubnt'):
        """ use get_cloudkey_mac to get the MAC address (hwaddr)
            of the cloudkey
        """
        import re
        command = '/sbin/ubnt-tools hwaddr'
        result = self.get_results_ssh_command(host, username, password, command)
        if result is False:
            return False
        for r in result:
            if re.search('^(?:[0-9a-f]{2}:){5}[0-9a-f]{2}$', r.strip()):
                return r.strip()
        return False

    def upgrade_cloudkey(self, host, user='ubnt', password='ubnt'):
        """ use upgrade_cloudkey to send the quick upgrade
            command to a cloudkey. if the device (currently the gen2 CK)
            is not supported with this command, look up the firmware file
            using the API and tell the cloudkey to load it
        """
        import re
        command = '/sbin/unifi-fwupdater.sh'
        result = self.get_results_ssh_command(host, user, password, command)
        if result is False:
            return False
        r1 = re.compile('^platform (?:[A-Z0-9]+) not supported yet')
        if r1.search(result[0].strip()):
            #print("DEBUG: unsupported platform for auto upgrade") # debug
            dl_link = self.check_new_device_upgrade(host)
            command = '/sbin/ubnt-systool fwupdate ' + dl_link
            result = self.get_results_ssh_command(host, user, password, command)
            if result is not False:
                for r in result:
                    if r.strip() == 'Firmware ready - rebooting to update...':
                        return True
            return False
        r2 = re.compile('No such file or directory')
        if r2.search(result[0].strip()):
            #print("DEBUG: auto upgrade script missing") # debug
            dl_link = self.check_new_device_upgrade(host)
            command = '/sbin/ubnt-systool fwupdate ' + dl_link
            result = self.get_results_ssh_command(host, user, password, command)
            if result is not False:
                for r in result:
                    if r.strip() == 'Firmware ready - rebooting to update...':
                        return True
            return False
        print("ERROR: Unable to confirm if upgrade_cloudkey() was successful")
        for r in result:
            print(r)
        return False

    def discover(self):
        """ use SSDP to discover new cloudkey
            mx value allows the root device to wait a
               random time between 0 and the mx value
               before responding, allowing for large
               network to not DoS the client. should
               be 1 to 5. set to lower value 1 when
               in a virtually empty network.
            returns dictionary with
               ip
               version
               path
        """
        import re
        import socket
        cloudkeys = [] # discovered cloudkeys
        message = "\r\n".join(['M-SEARCH * HTTP/1.1',
                               'HOST: 239.255.255.250:1900',
                               'MAN: "ssdp:discover"',
                               'ST: upnp:rootdevice',
                               "MX: {}".format(self.ssdp_mx)])
        # UDP socket
        s = socket.socket(socket.AF_INET,
                          socket.SOCK_DGRAM,
                          socket.IPPROTO_UDP)
        s.settimeout(self.ssdp_mx + 1) # allow for slow device or congestion
        s.sendto(message.encode('utf-8'), ('239.255.255.250', 1900))
        try:
            while True:
                xml_path = False
                unifi_version = False
                unifi_ip = False
                data, addr = s.recvfrom(65507)
                ip, dummy_port = addr
                contents = data.decode(encoding='utf-8').split('\r\n')
                server_match = re.compile('^Server: ?([a-z0-9. /]+)',
                                          re.IGNORECASE)
                unifi_match = re.compile('^UniFi/([0-9./]+)$',
                                         re.IGNORECASE)
                xml_match = re.compile('^Location: ?(.+)$',
                                       re.IGNORECASE)
                for item in contents:
                    server_matches = server_match.search(item)
                    if server_matches:
                        servers = server_matches.group(1).split(' ')
                        for server in servers:
                            unifi_matches = unifi_match.search(server)
                            if unifi_matches:
                                unifi_version = unifi_matches.group(1)
                                unifi_ip = ip
                    xml_matches = xml_match.search(item)
                    if xml_matches:
                        xml_path = xml_matches.group(1)
                if False not in [unifi_ip, unifi_version, xml_path]:
                    result = {'ip': unifi_ip,
                              'version': unifi_version,
                              'path': xml_path}
                    result['path_readable'] = self.test_ssdp_location(xml_path)
                    result['default_creds'] = self.test_cloudkey_default_ssh_credentials(unifi_ip)
                    upgrade = self.check_new_device_upgrade(unifi_ip)
                    if upgrade is False:
                        result['upgradeable'] = False
                    else:
                        result['upgradeable'] = True
                        result['upgrade_link'] = upgrade
                    cloudkeys.append(result)
        except socket.timeout:
            pass
        self.cloudkeys = []
        for cloudkey in cloudkeys:
            self.cloudkeys.append(cloudkey)
        return cloudkeys

    def set_controller_admin(self, ip, name, email, password, port=8443):
        """ use set_controller_admin to help set up a new
            CloudKey by setting the default admin, password,
            and the email address to use

            this overrides the functions set by:
              set_cloudkey_admin_name()
              set_cloudkey_admin_email()
              set_cloudkey_admin_password()
        """
        posturl = "https://{}:{}/api/cmd/sitemgr".format(ip, port)
        data = {'cmd': 'add-default-admin',
                'name': name,
                'email': email,
                'x_password': password}
        return self._post_api(posturl, data, False)

    def set_controller_change_ssh_username(self, ip, username, port=8443):
        """ use set_controller_change_ssh_username to
            help set up a new CloudKey by setting the ssh username
        """
        posturl = "https://{}:{}/api/cmd/system".format(ip, port)
        data = {'cmd': 'change-ssh-username',
                'username': username}
        return self._post_api(posturl, data, False)

    def set_controller_change_ssh_password(self, ip, password, port=8443):
        """ use set_controller_change_ssh_password to
            help set up a new CloudKey by setting the
            ssh password
        """
        posturl = "https://{}:{}/api/cmd/system".format(ip, port)
        data = {'cmd': 'change-ssh-password',
                'password': password}
        return self._post_api(posturl, data, False)

    def check_controller_ssh_credentials(self, ip, username, password,
                                         port=8443):
        """ use check_controller_ssh_credentials to
            test ssh credentials using the API
        """
        posturl = "https://{}:{}/api/cmd/system".format(ip, port)
        data = {'cmd': 'check-ssh-credentials',
                'username': username,
                'password': password}
        return self._post_api(posturl, data, False)

    def set_super_identity(self, ip, sid, port=8443):
        """ use set_super_identity to help set up the CloudKey
            by setting the super identity name, which is the
            controller name under controller settings
        """
        posturl = "https://{}:{}/api/set/setting/super_identity".format(ip, port)
        data = {'name': sid}
        return self._post_api(posturl, data, False)

    def set_country(self, ip, country=840, port=8443):
        """ use set_country to help set up a CloudKey by
            setting the country to the default 840, United States
            https://en.wikipedia.org/wiki/List_of_ISO3166_country_codes
        """
        posturl = "https://{}:{}/api/set/setting/country".format(ip, port)
        data = {'code': country}
        return self._post_api(posturl, data, False)

    def set_timezone(self, ip, locale='America/New_York', port=8443):
        """ use set_timezone to help set up a CloudKey by
            setting the timezone to the default America/New_York
            https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
        """
        posturl = "https://{}:{}/api/set/setting/locale".format(ip, port)
        data = {'timezone': locale}
        return self._post_api(posturl, data, False)

    def set_network_optimization(self, ip, enabled=True, port=8443):
        """ use set_network_optimization to help set up a CloudKey by
            setting network optimization
        """
        posturl = "https://{}:{}/api/set/setting/network_optimization".format(ip, port)
        data = {'enabled': enabled}
        return self._post_api(posturl, data, False)

    def enable_backups(self, ip, auto=True, cloud=True, port=8443):
        """ use enable_backups to help set up a CloudKey by
            setting up backups
        """
        posturl = "https://{}:{}/api/set/setting/super_mgmt".format(ip, port)
        data = {'autobackup_enabled': auto,
                'autobackup_cron_expr': '0 0 1 * *',
                'autobackup_timezone': 'UTC',
                'autobackup_days': 30,
                'backup_to_cloud_enabled': cloud}
        return self._post_api(posturl, data, False)

    def set_device_ssh_credentials(self, ip, username, password, port=8443):
        """ use set_device_ssh_credentials to help set up a
            CloudKey by setting up the ssh credentials used to
            connect to managed devices such as access points
        """
        posturl = "https://{}:{}/api/set/setting/mgmt".format(ip, port)
        data = {'x_ssh_username': username,
                'x_ssh_password': password}
        return self._post_api(posturl, data, False)

    def set_installed_flag(self, ip, port=8443):
        """ use set_installed_flag to help set up a CloudKey
            by completing the inital unauthenticated setup
            and committing the settings and starting
            controller operation, allowing normal local
            logins
        """
        posturl = "https://{}:{}/api/cmd/system".format(ip, port)
        data = {'cmd': 'set-installed'}
        return self._post_api(posturl, data, False)

if __name__ == '__main__':
    # connect one or more cloudkeys set to factory defaults to the network
    # if you connect more than one cloudkey, each will be tested
    # this is good to test with gen1, gen2, gen2plus cloudkeys
    # for a complete test, before testing, downgrade firmware on each
    # device so the upgrade process can be tested
    print('starting tests')
    test_email = 'youremail@provider.net'
    api = pyCloudkeyApi()
    print("discovering CloudKeys...")
    keys = api.discover()
    assert(keys), 'failed test discover(), ensure you have a CloudKey set to default'
    for ckey in keys:
        print('test starting for CloudKey {}'.format(ckey['ip']))
        key_ip = ckey['ip']
        key_info = api.get_controller_info(key_ip)
        assert(key_info), 'failed get_controller_info()'
        key_version = ckey['version']
        key_location = ckey['path']
        key_location_readable = ckey['path_readable']
        assert(api.test_cloudkey_default_ssh_credentials(key_ip)), 'failed test test_cloudkey_default_ssh_credentials'
        key_model, key_version, key_firmware = api.test_cloudkey_firmware_default(key_ip)
        assert(key_model and key_version and key_firmware), 'failed test_cloudkey_firmware_default'
        key_latest_firmware, filename = api.get_latest_firmware_info(key_model)
        assert(key_latest_firmware), 'failed test get_latest_firmware_info, is the internet reachable?'
        upgrade_check = api.check_new_device_upgrade(key_ip)
        if upgrade_check:
            print("upgrading {} -> {}".format(key_version, key_latest_firmware))
            assert(api.upgrade_cloudkey(key_ip) is True), 'failed test upgrade_cloudkey'
            assert(api.isup(key_ip) is True), 'failed to come back up after upgrade_cloudkey'
        assert(api.set_cloudkey_name(key_ip, 'cloudkey_test')), 'failed test set_cloudkey_name'
        assert(api.set_cloudkey_admin_name(key_ip, 'localadmin')), 'failed test set_cloudkey_admin_name'
        assert(api.set_cloudkey_admin_email(key_ip, test_email, 'localadmin')), 'failed test set_cloudkey_admin_email'
        assert(api.set_cloudkey_timezone(key_ip, 'EST', 'localadmin')), 'failed test set_cloudkey_timezone'
        assert(api.set_cloudkey_admin_password(key_ip, 'password', 'localadmin', 'ubnt')), 'failed test set_cloudkey_admin_password'
        assert(api.set_cloudkey_reset_button(key_ip, False, 'localadmin', 'password')), 'failed set_cloudkey_reset_button off'
        assert(api.set_cloudkey_reset_button(key_ip, True, 'localadmin', 'password')), 'failed set_cloudkey_reset_button on'
        assert(api.get_cloudkey_mac(key_ip, 'localadmin', 'password') is not False), 'failed test get_cloudkey_mac'
        assert(api.set_cloudkey_led(key_ip, 'localadmin', 'password', 'all', 'on')), 'failed test set_cloudkey_led all on'
        assert(api.set_cloudkey_led(key_ip, 'localadmin', 'password', 'blue', 'off')), 'failed test set_cloudkey_led blue off'
        assert(api.set_cloudkey_led(key_ip, 'localadmin', 'password', 'white', 'off')), 'failed test set_cloudkey_led white off'
        assert(api.set_cloudkey_led(key_ip, 'localadmin', 'password', 'blue', 'settimer', 255, 50, 610)), 'failed test set_cloudkey_led blue blink'
        assert(api.set_cloudkey_led(key_ip, 'localadmin', 'password', 'white', 'on', -1) is False), 'failed test set_cloudkey_led invalid brightness'
        assert(api.set_cloudkey_led(key_ip, 'localadmin', 'password', 'red', 'on') is False), 'failed test set_cloudkey_led invalid color'
        assert(api.reboot_cloudkey(key_ip, 'localadmin', 'password')), 'failed test reboot_cloudkey'
        assert(api.isup(key_ip, 'localadmin', 'password') is True), 'failed to come up after reboot_cloudkey'
        assert(api.reset_cloudkey(key_ip, 'localadmin', 'password')), 'failed test reset_cloudkey'
        assert(api.isup(key_ip) is True), 'failed to come up after reset_cloudkey'
        # set up controller similar to wizard advanced mode
        assert(api.set_controller_admin(key_ip, 'localadmin', test_email, 'password') is True), 'failed test set_controller_admin'
        assert(api.set_controller_change_ssh_username(key_ip, 'localadmin') is True), 'failed test set_controller_change_ssh_username'
        assert(api.set_controller_change_ssh_password(key_ip, 'password') is True), 'failed test set_controller_ssh_password'
        assert(api.check_controller_ssh_credentials(key_ip, 'localadmin', 'password') is True), 'fail test confirm ssh credentials changed check_controller_ssh_credentials'
        assert(api.get_creds(key_ip) is not False), 'failed test get_creds()'
        assert(api.set_super_identity(key_ip, 'controllername') is True), 'failed test set_super_identity'
        assert(api.set_country(key_ip) is True), 'failed test set_country'
        assert(api.set_timezone(key_ip) is True), 'failed test set_timezone'
        assert(api.set_network_optimization(key_ip, False) is True), 'failed test set_network_optimization'
        assert(api.enable_backups(key_ip, True, False) is True), 'failed test enable_backups'
        assert(api.set_device_ssh_credentials(key_ip, 'devadmin', 'password') is True), 'failed test set_device_ssh credentials'
        # set-installed status completes the 'wizard' so we can now manage with pyunifiapi.py
        assert(api.set_installed_flag(key_ip) is True), 'failed test set_installed_flag'
        # reset to defaults since our tests are complete
        assert(api.reset_cloudkey(key_ip, 'localadmin', 'password')) or 'failed test reset_cloudkey()'
        assert(api.isup(key_ip) is True), ('failed to come up after final reset '
                                           'to defaults of reset_cloudkey')
        print('test complete for CloudKey ' + key_ip)
    print('tests complete')