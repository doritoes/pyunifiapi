#!/usr/bin/env python3
"""
    rolling-upgrade.py
      for all adopted APs round, do a rolling upgrade

    TODO add check for connections, defer if there are connections
    TODO check if there are at least 2 APs up, fail if only 1
         to prevent totally taking down internet
    TODO retrieve credentials from user, not variables
"""
from pyunifiapi import pyUnifiAPI

prep_unifi_username = 'localadmin2'
prep_unifi_password = 'password2'
prep_site = 'default'
test_unifi = '192.168.99.135'

api = pyUnifiAPI(prep_unifi_username,
                 prep_unifi_password,
                 test_unifi,
                 8443,
                 False)
print('Rolling upgrade of APs')
sites = api.get_sites()
for site in sites:
    aps = api.get_devices(site, 'uap')
    for ap in aps:
        mac = ap['mac']
        result = api.get_device_by_mac(site, mac)
        if 'name' in result[0]:
            name = result[0]['name']
        else:
            name = result[0]['serial']
        if result[0]['adopted'] is False:
            continue
        if result[0]['state'] != 1:
            if result[0]['state'] in [2, 10]:
                api.wait_status(site, mac, 1)
            else:
                print("Skipping {} => {}".format(site, name))
                continue
        print("{} => {}".format(site, name))
        if result[0]['upgradable']:
            if api.upgrade_device(site, mac) is True:
                print('upgraded firmware')
                api.wait_status(site, mac, 1)
            else:
                print('failed to upgrade firmware')
print('Program complete.')