#!/usr/bin/env python3
""" downgrade-cloudkey.py
      script to downgrade CloudKey firmware to a previous
      version using the Ubiquiti web site links

      be sure to reset the CloudKey(s) to factory defaults first
"""
import re
import sys
from pycloudkeyapi import pyCloudkeyApi
worklist = []
discovered = []
api = pyCloudkeyApi()
for a in sys.argv:
    if a == 'all':
        print('Discovering CloudKeys...')
        allkeys = api.discover()
        for k in allkeys:
            print('discovered: {}'.format(k['ip']))
            if k['ip'] not in discovered:
                discovered.append(k['ip'])
    else:
        if a not in worklist:
            worklist.append(a)
worklist.pop(0)
for d in discovered:
    if d not in worklist:
        worklist.append(d)
if not worklist:
    print('Make sure the CloudKey is already set to factory defaults')
    worklist.append(input('IP of CloudKey to downgrade: '))
for w in worklist:
    model, version, firmware = api.test_cloudkey_firmware_default(w)
    if False in (model, version, firmware):
        print('{} does not look like a CloudKey, or it is not up'.format(w))
        continue
    version_data = api.get_previous_firmware_info(model)
    # to get older versions, use (model, #) where # is the number of
    # versions back you want to go
    # my test UCK hardsware with USB-C power doesn't like downgrading -
    #   "doesn't fit the system!"
    if version_data is False:
        print('There are no images that many versions ago for model {}.'.format(model))
        sys.exit()
    older_version, image_to_load = version_data
    print('Trying a downgrade on CloudKey ' + w)
    print('Model: {}'.format(model))
    print('Current version (firmware): {} ({})'.format(version, firmware.strip()))
    print('Applying version {} - {}'.format(older_version, image_to_load))
    print('Please be patient as large file is downloaded, unpacked, and installed')
    command = '/sbin/ubnt-systool fwupdate ' + image_to_load
    result = api.get_results_ssh_command(w, 'ubnt', 'ubnt', command)
    endresult = False
    if result is False:
        print('FAILED')
        endresult = False
    else:
        for r in result:
            if r.strip() == 'Firmware ready - rebooting to update...':
                print('SUCCEEDED\nPlease wait for the downgrade to complete')
                endresult = True
                assert(api.isup(w)), 'Device failed to come up after upgrade'
                print('CloudKey {} is back up'.format(w))
    if endresult is False:
        print('The recommended firmware failed')
        print('Failure details:')
        for r in result:
            if re.search(r'ERROR', r):
                print(r)
print('Program complete.')