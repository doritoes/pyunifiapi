# pyunifiapi

Basic python3 wrappers to the APIs for Ubiquiti Unifi conntroller and CloudKey

Credit goes to all those who have worked on this before me.

*  https://ubntwiki.com/products/software/unifi-controller/api
*  https://dl.ui.com/unifi/5.12.35/unifi_sh_api
*  https://github.com/Art-of-WiFi/UniFi-API-client
*  https://github.com/Art-of-WiFi/UniFi-API-browser
    *  be sure to check out the amazing live demo page!
*  https://medium.com/tenable-techblog/exploring-the-ubiquiti-unifi-cloud-key-gen2-plus-f5b0f7ca688

# Purpose

This is not currently intended to be a complete implementation of Unifi controller and CloudKey  
APIs, but to provide an easy interface for Python scripting for automation.

## Applications

Rotate guest wifi passwords, create vouchers, do backups, and more.

Easy preparation of a batch of CloudKey devices and deploying configurations to gear before deploying to the field is possible.

Updating firmware of a batch of devices one the bench before deploying to the field.

Perform a rolling upgrade of APs only, not including switches, which is how Unifi does it

# Setup

The easiest way is to install the Unifi controller on a Raspberry Pi or another
Linux server. Then you can run thess APIs on the same device.
