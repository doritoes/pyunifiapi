#!/usr/bin/env python3
""" adopt-cloudkey.py
      script to adopt a CloudKey by completing a basic
      advanced setup, after which configuration with
      pyunifiapi.py is possible.

      resets the CloudKey to default values, so don't
      do this to a CloudKey you already have set up
"""
import sys
import getpass
from pycloudkeyapi import pyCloudkeyApi

# set up local variables
cloudkey_username = 'localadmin'
cloudkey_password = 'password'
unifi_username = 'localadmin2'
unifi_password = 'password2'
unifi_email = 'youremail@provider.net'
dev_ssh_username = 'devadmin'
dev_ssh_password = 'password'

# set up variables to process worklist
worklist = []
discovered = []
failed = []

# instantiate api
api = pyCloudkeyApi()

# process arguments for CloudKeys to adopt
for a in sys.argv:
    if a == 'all':
        print('Discovering CloudKeys...')
        allkeys = api.discover()
        for k in allkeys:
            print('discovered: {}'.format(k['ip']))
            if k['ip'] not in discovered:
                discovered.append(k['ip'])
    else:
        if a not in worklist:
            worklist.append(a)
worklist.pop(0)
for d in discovered:
    if d not in worklist:
        worklist.append(d)
# if none specified, prompt user
if not worklist:
    worklist.append(input('IP of CloudKey to adopt: '))

# offer default passwords to use or modify
cloudkey_username = input('CloudKey admin: [{}] '.format(cloudkey_username)) or cloudkey_username
cloudkey_password = input('CloudKey password: [{}] '.format(cloudkey_password)) or cloudkey_password
unifi_username = input('Unifi username: [{}] '.format(unifi_username)) or unifi_username
unifi_password = input('Unifi password: [{}] '.format(unifi_password)) or unifi_password
unifi_email = input('Unifi email: [{}] '.format(unifi_email)) or unifi_email
dev_ssh_username = input('Unifi device ssh username: [{}] '.format(dev_ssh_username)) or dev_ssh_username
dev_ssh_password = input('Unifi device ssh password: [{}] '.format(dev_ssh_password)) or dev_ssh_password

if not input("Continue? (y/N)").lower() in ('y', 'yes'):
    sys.exit()

# process each CloudKey specified for each step
for w in worklist: # reset to factory defaults
    print('Resetting CloudKey with IP {} to factory defaults'.format(w))
    creds = api.get_creds(w)
    if creds is False:
        print('enter credentials')
        username = input('Username: ')
        password = getpass.getpass()
    else:
        username, password = creds
    print('Resetting {}'.format(w))
    if not api.reset_cloudkey(w, username, password):
        failed.append(w)
        print('Failed to reset CloudKey {} to factory defaults, skipping'.format(w))
        continue
for w in worklist: # upgrade CloudKey firmware as needed
    if w in failed:
        continue
    if not api.isup(w):
        print("CloudKey {} failed to come up after reset to factory defaults".format(w))
        failed.append(w)
        continue
    # upgrade firmware
    if api.check_new_device_upgrade(w, username, password) is False:
        continue
    print('upgrading {}'.format(w))
    if api.upgrade_cloudkey(w, username, password) and api.isup(w, username, password):
        print('Upgrade completed and {} is back online'.format(w))
    else:
        print('Upgrade failed for {}'.format(w))
        failed.append(w)
        continue
for w in worklist: # adopt
    if w in failed:
        continue
    print('Adopting: {}'.format(w))
    devicemac = api.get_cloudkey_mac(w)
    if devicemac is False:
        devicename = 'cloudkey-' + w.split('.')[-1]
    else:
        devicename = 'cloudkey-' + devicemac.replace(':', '').upper()[:-6]
    # set up controller similar to wizard in advanced mode
    if api.set_controller_admin(w, unifi_username, unifi_email, unifi_password) is False:
        failed.append(w)
        print('[!] Failed to set controller admin name: {}'.format(unifi_username))
        continue
    if api.set_controller_change_ssh_username(w, cloudkey_username) is False:
        failed.append(w)
        print('[!] Failed to set CloudKey ssh username')
        continue
    if api.set_controller_change_ssh_password(w, cloudkey_password) is False:
        failed.append(w)
        print('[!] Failed to set CloudKey ssh password')
    if api.check_controller_ssh_credentials(w, cloudkey_username, cloudkey_password) is False:
        print('[!] Check of new controller ssh credentials failed')
        failed.append(w)
        continue
    if api.set_super_identity(w, devicename) is False:
        failed.append(w)
        continue
    if api.set_country(w) is False:
        failed.append(w)
        print('[!] Failed to set country')
        continue
    if api.set_timezone(w) is False:
        failed.append(w)
        print('[!] Failed to set timezone')
        continue
    if api.set_network_optimization(w, False) is False:
        failed.append(w)
        print('[!] Failed to disable network optimization')
        continue
    if api.enable_backups(w, True, False) is False:
        failed.append(w)
        print('[!] Failed to enable automatic local backups (only)')
        continue
    if api.set_device_ssh_credentials(w, dev_ssh_username, dev_ssh_password) is False:
        failed.append(w)
        print('[!] Failed to set device ssh admin credentials')
        continue
    # complete the wizard
    if api.set_installed_flag(w) is False:
        failed.append(w)
        print('[!] Failed to finalized setup wizard')
        continue
    print("Login to Cloudkey {}: {}/{}".format(devicename, cloudkey_username, cloudkey_password))
    print("    ssh to {}".format(w))
    print("    https://{}".format(w))
    print("Login to the Unifi controller {}: {}/{}".format(devicename, unifi_username, unifi_password))
    print("    https://{}:8443".format(w))
    print('Done working on {} ({})'.format(devicename, w))

if failed:
    print('Failed to adopt the following CloudKeys:')
    for w in failed:
        print("[+] {}".format(w))
print('Program complete.')