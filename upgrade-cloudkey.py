#!/usr/bin/env python3
""" upgrade-cloudkey.py
      script to upgrade CloudKey firmware to the
      latest version using the Ubiquiti web site links
"""
import sys
import getpass
from pycloudkeyapi import pyCloudkeyApi
worklist = []
discovered = []
api = pyCloudkeyApi()
for a in sys.argv:
    if a == 'all':
        print('Discovering CloudKeys...')
        allkeys = api.discover()
        for k in allkeys:
            print('discovered: {}'.format(k['ip']))
            if k['ip'] not in discovered:
                discovered.append(k['ip'])
    else:
        if a not in worklist:
            worklist.append(a)
worklist.pop(0)
for d in discovered:
    if d not in worklist:
        worklist.append(d)
if not worklist:
    worklist.append(input('IP of CloudKey to upgrade: '))
for w in worklist:
    print('Working on CloudKey with IP {}'.format(w))
    creds = api.get_creds(w)
    if creds is False:
        print('enter credentials')
        username = input('Username: ')
        password = getpass.getpass()
    else:
        username, password = creds
    if api.check_new_device_upgrade(w, username, password) is False:
        print('No upgrade required, or no CloudKey found')
        continue
    if api.upgrade_cloudkey(w, username, password) and api.isup(w, username, password):
        print('Upgrade completed and {} is back online'.format(w))
    else:
        print('Upgrade failed for {}'.format(w))
    print('Done working on {}'.format(w))
print('Program complete.')