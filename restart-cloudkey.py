import sys
import getpass
from pycloudkeyapi import pyCloudkeyApi
worklist = []
discovered = []
api = pyCloudkeyApi()
for a in sys.argv:
    if a == 'all':
        print('Discovering CloudKeys...')
        allkeys = api.discover()
        for k in allkeys:
            print('discovered: {}'.format(k['ip']))
            if k['ip'] not in discovered:
                discovered.append(k['ip'])
    else:
        if a not in worklist:
            worklist.append(a)
worklist.pop(0)
for d in discovered:
    if d not in worklist:
        worklist.append(d)
if not worklist:
    worklist.append(input('IP of CloudKey to reset to restart: '))
for w in worklist:
    print('Restarting CloudKey: ' + w)
    creds = api.get_creds(w)
    if creds is False:
        print('enter credentials')
        username = input('Username: ')
        password = getpass.getpass()
    else:
        username, password = creds
    result = api.reboot_cloudkey(w, username, password)
    if result is True:
        print('Sent reboot command')
    else:
        print('Failed to send reboot command')
print('Waiting for device(s) to come back up')
for w in worklist:
    if api.isup(w, username, password) is False:
        print('CloudKey failed to come back up after restart: {}'.format(w))
print('Program complete.')